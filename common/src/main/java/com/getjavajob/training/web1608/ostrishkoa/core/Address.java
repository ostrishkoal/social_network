package com.getjavajob.training.web1608.ostrishkoa.core;

import com.getjavajob.training.web1608.ostrishkoa.core.additions.AddressType;

public class Address {
    private String address;
    private AddressType type;

    public Address() {
    }

    public Address(String address) {
        this.address = address;
    }

    public String getAddress() {

        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type == null ? null : type.name();
    }

    public void setType(String type) {
        if (type != null) {
            this.type = AddressType.valueOf(type);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (address != null ? address.hashCode() : 0);
        hash += (type != null ? type.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Address)) {
            return false;
        }
        Address other = (Address) object;
        if (address == null && other.getAddress() != null || address != null && !address.equals(other.getAddress())) {
            return false;
        }
        if (type == null && other.getType() != null || type != null && !type.name().equals(other.getType())) {
            return false;
        }
        return true;
    }
}
