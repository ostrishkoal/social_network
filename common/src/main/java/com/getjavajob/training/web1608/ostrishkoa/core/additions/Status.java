package com.getjavajob.training.web1608.ostrishkoa.core.additions;

public enum Status {
    DEFAULT, PENDING, ACCEPTED;
}
