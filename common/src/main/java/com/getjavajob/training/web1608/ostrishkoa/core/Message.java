package com.getjavajob.training.web1608.ostrishkoa.core;

import javax.persistence.*;

@Entity
@Table(name = "messages")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "account_from_id")
    private Account accountFrom;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "account_to_id")
    private Account accountTo;
    private String message;

    public Message(Account accountFrom, Account accountTo, String message) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.message = message;
    }

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Account getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(Account accountFrom) {
        this.accountFrom = accountFrom;
    }

    public Account getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(Account accountTo) {
        this.accountTo = accountTo;
    }

    @Override
    public String toString() {
        return "Message{" + "id=" + id + ", accountFrom=" + accountFrom + ", accountTo=" + accountTo + ", message='" + message + '\'' + '}';
    }
}
