package com.getjavajob.training.web1608.ostrishkoa.core;

import com.getjavajob.training.web1608.ostrishkoa.core.additions.*;
import com.getjavajob.training.web1608.ostrishkoa.core.relations.*;
import com.thoughtworks.xstream.annotations.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.xml.bind.DatatypeConverter;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Stream.concat;

@Entity
@Table(name = "ref_account")
@XStreamAlias("account")
public class Account implements Identity<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XStreamOmitField
    private Integer id;
    @XStreamOmitField
    private String uuid;
    private String name;
    private String surname;
    @Temporal(value = TemporalType.DATE)
    @DateTimeFormat(pattern = "dd-mm-yyyy")
    @Column(name = "date_birth")
    private Date dateBirth;
    @Enumerated(EnumType.STRING)
    private Sex sex;
    @Column(unique = true)
    private String email;
    @Column(nullable = false)
    @XStreamOmitField
    private String password;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @XStreamImplicit(itemFieldName = "phone")
    @JoinTable(name = "phones", joinColumns = @JoinColumn(name = "account_id"), inverseJoinColumns = @JoinColumn(name = "phone_id"))
    private List<Phone> phones = new ArrayList<>();
    @XStreamOmitField
    private byte[] photo;
    @XStreamOmitField
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @XStreamOmitField
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "accountFrom")
    private Set<AccountRelation> relationsFrom = new HashSet<>();
    @XStreamOmitField
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "accountTo")
    private Set<AccountRelation> relationsTo = new HashSet<>();
    @XStreamOmitField
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "account")
    private Set<GroupRelation> groupsRelation = new HashSet<>();
    @XStreamOmitField
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "creator")
    private Set<Group> createdGroups = new HashSet<>();

    public Account() {
    }

    public Account(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Set<Group> getCreatedGroups() {
        return createdGroups;
    }

    public void setCreatedGroups(Set<Group> createdGroups) {
        this.createdGroups = createdGroups;
    }

    //    private List<Address> addresses = new ArrayList<>();
    //    private String icq;
    //    private String skype;
    //    private String hobby;
    //    private String education;
    //    private String employment;

    public Relation getRelation(Account account) {
        Optional<AccountRelation> optional = getAccountRelationTo(account);
        if (optional.isPresent()) {
            return new Relation(optional.get().getStatus(), false);
        }
        optional = getAccountRelationFrom(account);
        if (optional.isPresent()) {
            return new Relation(optional.get().getStatus(), true);
        }
        return new Relation(Status.DEFAULT, true);
    }

    public Optional<AccountRelation> getAccountRelationFrom(Account account) {
        return relationsFrom.stream().filter(accountRelation -> accountRelation.getAccountTo().equals(account)).findFirst();
    }

    public Optional<AccountRelation> getAccountRelationTo(Account account) {
        return relationsTo.stream().filter(accountRelation -> accountRelation.getAccountFrom().equals(account)).findFirst();
    }

    public Set<Group> getGroups() {
        return groupsRelation.stream().filter(groupRelation -> groupRelation.getStatus() == Status.ACCEPTED).map(GroupRelation::getGroup).collect(Collectors.toSet());
    }

    public Set<Group> getRequestedGroups() {
        return groupsRelation.stream().filter(groupRelation -> groupRelation.getStatus() == Status.PENDING).map(GroupRelation::getGroup).collect(Collectors.toSet());
    }

    public Set<Account> getFriends() {
        return concat(relationsFrom.stream().filter(accountRelation -> accountRelation.getStatus() == Status.ACCEPTED).map(AccountRelation::getAccountTo), relationsTo.stream().filter(accountRelation -> accountRelation.getStatus() == Status.ACCEPTED).map(AccountRelation::getAccountFrom)).collect(Collectors.toSet());
    }


    public Set<Account> getFollowed() {
        return relationsFrom.stream().filter(accountRelation -> accountRelation.getStatus() == Status.PENDING).map(AccountRelation::getAccountTo).collect(Collectors.toSet());
    }


    public Set<Account> getFollowers() {
        return relationsTo.stream().filter(accountRelation -> accountRelation.getStatus() == Status.PENDING).map(AccountRelation::getAccountFrom).collect(Collectors.toSet());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(Date date) {
        dateBirth = date;
    }

    public String getSex() {
        return sex == null ? null : sex.name();
    }

    public void setSex(String sex) {
        if (sex != null) {
            this.sex = Sex.valueOf(sex);
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public boolean removePhone(Phone phone) {
        return phones.remove(phone);
    }

    public boolean addPhone(Phone phone) {
        return phones.add(phone);
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoString() {
        return photo != null ? DatatypeConverter.printBase64Binary(photo) : null;
    }

    //    public String getHobby() {
    //        return hobby;
    //    }
    //
    //    public void setHobby(String hobby) {
    //        this.hobby = hobby;
    //    }
    //
    //    public String getEducation() {
    //        return education;
    //    }
    //
    //    public void setEducation(String education) {
    //        this.education = education;
    //    }
    //
    //    public String getEmployment() {
    //        return employment;
    //    }
    //
    //    public void setEmployment(String employment) {
    //        this.employment = employment;
    //    }

    //    public String getIcq() {
    //        return icq;
    //    }
    //
    //    public void setIcq(String icq) {
    //        this.icq = icq;
    //    }
    //
    //    public String getSkype() {
    //        return skype;
    //    }
    //
    //    public void setSkype(String skype) {
    //        this.skype = skype;
    //    }
    //
    //    public List<Address> getAddresses() {
    //        return addresses;
    //    }
    //
    //    public void setAddresses(List<Address> addresses) {
    //        this.addresses = addresses;
    //    }
    //    public boolean addAddress(Address address) {
    //        return addresses.add(address);
    //    }
    //
    //    public boolean removeAddress(Address address) {
    //        return addresses.remove(address);
    //    }

    public Set<AccountRelation> getRelationsFrom() {
        return relationsFrom;
    }

    public void setRelationsFrom(Set<AccountRelation> relationsFrom) {
        this.relationsFrom = relationsFrom;
    }

    public Set<AccountRelation> getRelationsTo() {
        return relationsTo;
    }

    public void setRelationsTo(Set<AccountRelation> relationsTo) {
        this.relationsTo = relationsTo;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<GroupRelation> getGroupsRelation() {
        return groupsRelation;
    }

    public void setGroupsRelation(Set<GroupRelation> groupsRelation) {
        this.groupsRelation = groupsRelation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (email != null ? email.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        if (email == null && other.getEmail() != null || email != null && !email.equals(other.getEmail())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Account[ id: " + id
                //                + "; name: " + name
                //                + "; date: " + dateBirth
                //                + "; email: " + email
                //                + "; pass: " + password
                //                + "; phones: " + Arrays.toString(phones.toArray())
                + " ]";
    }
}
