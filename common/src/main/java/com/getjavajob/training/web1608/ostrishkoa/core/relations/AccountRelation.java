package com.getjavajob.training.web1608.ostrishkoa.core.relations;

import com.getjavajob.training.web1608.ostrishkoa.core.Account;
import com.getjavajob.training.web1608.ostrishkoa.core.additions.Status;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "account_relation")
public class AccountRelation implements Serializable {
    @Enumerated(EnumType.STRING)
    private Status status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_from_id")
    private Account accountFrom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_to_id")
    private Account accountTo;

    public AccountRelation() {
    }

    public AccountRelation(Status status, Account accountFrom, Account accountTo) {
        this.status = status;
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Account getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(Account accountFrom) {
        this.accountFrom = accountFrom;
    }

    public Account getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(Account accountTo) {
        this.accountTo = accountTo;
    }


    @Override
    public String toString() {
        return "id: " + id
                + " from: " + accountFrom
                + " to: " + accountTo
                + " status : " + status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountRelation that = (AccountRelation) o;
        if (accountFrom != null ? !accountFrom.equals(that.accountFrom) : that.accountFrom != null) {
            return false;
        }
        return accountTo != null ? accountTo.equals(that.accountTo) : that.accountTo == null;

    }

    @Override
    public int hashCode() {
        int result = accountFrom != null ? accountFrom.hashCode() : 0;
        result = 31 * result + (accountTo != null ? accountTo.hashCode() : 0);
        return result;
    }

    public void swap() {
        Account account = accountFrom;
        accountFrom = accountTo;
        accountTo = account;
    }
}
