package com.getjavajob.training.web1608.ostrishkoa.core.additions;

public interface Identity<K> {
    K getId();

    void setId(K key);
}
