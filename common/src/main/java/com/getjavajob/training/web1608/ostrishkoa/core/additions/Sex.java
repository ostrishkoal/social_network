package com.getjavajob.training.web1608.ostrishkoa.core.additions;

public enum Sex {
    F, M;
}
