package com.getjavajob.training.web1608.ostrishkoa.core.additions;

public class Relation {
    private Status status;
    private boolean isOwner;

    public Relation(Status status, boolean isOwner) {
        this.status = status;
        this.isOwner = isOwner;
    }

    public Relation() {
    }

    public boolean isOwner() {
        return isOwner;
    }

    public void setOwner(boolean owner) {
        isOwner = owner;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Relation{" + "status=" + status + ", isOwner=" + isOwner + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Relation relation = (Relation) o;
        if (isOwner != relation.isOwner) {
            return false;
        }
        return status == relation.status;
    }

    @Override
    public int hashCode() {
        int result = status != null ? status.hashCode() : 0;
        result = 31 * result + (isOwner ? 1 : 0);
        return result;
    }
}
