package com.getjavajob.training.web1608.ostrishkoa.core;

import com.getjavajob.training.web1608.ostrishkoa.core.additions.PhoneType;
import com.thoughtworks.xstream.annotations.*;

import javax.persistence.*;

@Entity
@Table(name = "ref_phone")
@XStreamAlias("number")
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XStreamOmitField
    private Integer id;
    private String number;
    @Enumerated(EnumType.STRING)
    private PhoneType type;

    public Phone(String phone) {
        this.number = phone;
    }

    public Phone(String phone, String type) {
        this.number = phone;
        this.type = PhoneType.valueOf(type);
    }

    public Phone() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type == null ? null : type.toString();
    }

    public void setType(String type) {
        if (type != null) {
            this.type = PhoneType.valueOf(type);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (number != null ? number.hashCode() : 0);
        hash += (type != null ? type.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Phone)) {
            return false;
        }
        Phone other = (Phone) object;
        if (number == null && other.getNumber() != null || number != null && !number.equals(other.getNumber())) {
            return false;
        }
        if (type == null && other.getType() != null || type != null && !type.name().equals(other.getType())) {
            return false;
        }
        return true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "number: " + number + ", type: " + type;
    }
}
