package com.getjavajob.training.web1608.ostrishkoa.core;

import com.getjavajob.training.web1608.ostrishkoa.core.additions.*;
import com.getjavajob.training.web1608.ostrishkoa.core.relations.*;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import javax.persistence.*;
import javax.xml.bind.DatatypeConverter;
import java.util.*;
import java.util.stream.*;

@Entity
@Table(name = "ref_group")
public class Group implements Identity<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false)
    private String name;
    private String description;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "creator") //todo tests
    private Account creator;
    private byte[] photo;

    @XStreamOmitField
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "group")
    private Set<GroupRelation> groupRelations = new HashSet<>();

    public Group() {
    }

    public Group(String name, Account creator) {
        this.name = name;
        this.creator = creator;
    }

    public Group(String name) {
        this.name = name;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoString() {
        return photo != null ? DatatypeConverter.printBase64Binary(photo) : null;
    }

    public Status getRelationStatus(Account account) {
        Optional<GroupRelation> optional = getGroupRelation(account);
        if (optional.isPresent()) {
            System.out.println("getRelationStatus: " + optional.get().getStatus());
            return optional.get().getStatus();
        }
        return Status.DEFAULT;
    }

    public Role getRelationRole(Account account) {
        Optional<GroupRelation> optional = getGroupRelation(account);
        if (optional.isPresent()) {
            return optional.get().getRole();
        }
        return Role.USER;
    }

    public Optional<GroupRelation> getGroupRelation(Account account) {
        return groupRelations.stream().filter(accountRelation -> accountRelation.getAccount().equals(account)).findFirst();
    }

    public Set<Account> getMembers(){
        return groupRelations.stream().filter(groupRelation -> groupRelation.getStatus() == Status.ACCEPTED).map(GroupRelation::getAccount).collect(Collectors.toSet());
    }

    public Set<Account> getFollowers(){
        return groupRelations.stream().filter(groupRelation -> groupRelation.getStatus() == Status.PENDING).map(GroupRelation::getAccount).collect(Collectors.toSet());
    }

    public Set<GroupRelation> getGroupRelations() {
        return groupRelations;
    }

    public void setGroupRelations(Set<GroupRelation> groupRelations) {
        this.groupRelations = groupRelations;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Group)) {
            return false;
        }
        Group other = (Group) object;
        if (name == null && other.getName() != null || name != null && !name.equals(other.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Group[ id: " + id + "; name: " + name + "; description: " + description + "; creator: " + creator + " ]";
    }
}
