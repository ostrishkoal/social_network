Social network

** Functionality: **
+ registration/authentication
+ display profile
+ edit profile
+ upload and download avatar
+ ajax loading of accounts
+ ajax search with pagination
+ users export to xml
+ socket chat

** Tools: **
JDK 8, Spring 4, JPA 2 / Hibernate 5, XStream, jQuery 2, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Git/Bitbucket, Tomcat 7, MySQL/HSQLDB 2, IntelliJIDEA 16, log4j 2

** Screenshots **
https://i.gyazo.com/b5f7331ed6ed7e9b9de9dff0ff20b2b7.png
https://i.gyazo.com/9cdf7c675ef06f2cf21f039ab789d8aa.png

** Heroku **
https://gjjsocialnetwork.herokuapp.com
login: test@mail.ru
password: test

_  
**Ostrishko Aleksandr**  
Training getJavaJob  
http://www.getjavajob.com