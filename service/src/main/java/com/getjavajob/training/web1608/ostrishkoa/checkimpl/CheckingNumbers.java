package com.getjavajob.training.web1608.ostrishkoa.checkimpl;

class CheckingNumbers {
    static boolean checkNoPositiveNumber(double d) {
        return d <= 0;
    }

    static boolean compareTwoNumbers(double a, double b) {
        return a > b;
    }
}
