package com.getjavajob.training.web1608.ostrishkoa;

import com.getjavajob.training.web1608.ostrishkoa.core.*;
import com.getjavajob.training.web1608.ostrishkoa.dao.GroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupService {
    @Autowired
    private GroupDao groupDao;

    public GroupService(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    public List<Group> findByAccount(Account account) {
        return groupDao.findByAccount(account);
    }

    public Group findById(Integer id) {
        return groupDao.find(id);
    }

    @Transactional
    public void updateGroup(Group group) {
        groupDao.update(group);
    }

    @Transactional
    public void createGroup(Group group) {
        groupDao.create(group);
    }

    public List<Group> findByPartName(String name) {
        return groupDao.findByPartName(name);
    }

    public List<Group> findByPartName(String name, int cntLimit, int pageNamber) {
        return groupDao.findByPartName(name, cntLimit, pageNamber);
    }

    public Long getCntByPartName(String name) {
        return groupDao.getCntByPartName(name);
    }

    @Transactional
    public void sendGroupReq(Account account, Group group) {
        groupDao.sendGroupReq(account, group);
    }

    @Transactional
    public void acceptMember(Account account, Group group) {
        groupDao.acceptMember(account, group);
    }

    @Transactional
    public void declineMember(Account account, Group group) {
        groupDao.declineMember(account, group);
    }

    @Transactional
    public void setAdmin(Account account, Group group) {
        groupDao.setAdmin(account, group);
    }
}
