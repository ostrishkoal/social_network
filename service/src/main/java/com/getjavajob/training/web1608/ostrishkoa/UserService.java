package com.getjavajob.training.web1608.ostrishkoa;


import com.getjavajob.training.web1608.ostrishkoa.core.Account;
import com.getjavajob.training.web1608.ostrishkoa.core.additions.User;
import com.getjavajob.training.web1608.ostrishkoa.dao.AccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.*;

import java.util.*;

public class UserService implements UserDetailsService {
    @Autowired
    private AccountDao accountDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountDao.findByEmail(username);
        if (account != null) {
            return new User(username, account.getPassword(), getGrantedAuthorities(account), true, true, true, true);
        }
        throw new UsernameNotFoundException("user" + username + "not found");
    }

    //possibly many roles
    private List<GrantedAuthority> getGrantedAuthorities(Account account) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + account.getRole()));
        return authorities;
    }
}
