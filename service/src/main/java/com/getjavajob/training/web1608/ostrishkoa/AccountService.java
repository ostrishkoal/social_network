package com.getjavajob.training.web1608.ostrishkoa;

import com.getjavajob.training.web1608.ostrishkoa.checkimpl.*;
import com.getjavajob.training.web1608.ostrishkoa.core.Account;
import com.getjavajob.training.web1608.ostrishkoa.core.additions.Relation;
import com.getjavajob.training.web1608.ostrishkoa.dao.AccountDao;
import com.getjavajob.training.web1608.ostrishkoa.validation.Validator;
import com.getjavajob.training.web1608.ostrishkoa.validation.check.Check;
import com.getjavajob.training.web1608.ostrishkoa.validation.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static java.util.Arrays.asList;

@Service
public class AccountService {
    public static final List<Check<Account>> CHECK_LIST = new ArrayList<>(asList(
            //            new CheckFullName(),
            //            new CheckDate(),
            //            new CheckSex(),
            new CheckEmail(), new CheckPassword()
            //, new CheckPhones()
    ));
    private AccountDao accountDao;
    private Validator<Account> validator = new Validator<>(CHECK_LIST);
    @Autowired
    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public AccountService() {
    }

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Transactional
    public void createAccount(Account account) throws ServiceException {
        try {
            validator.validate(account);
            accountDao.create(account);
        } catch (ValidationException e) {
            throw new ServiceException(e);
        }
    }

    @Transactional
    public void updateAccount(Account account) throws ServiceException {
        try {
            validator.validate(account);
            accountDao.update(account);
        } catch (ValidationException e) {
            throw new ServiceException(e);
        }
    }

    @Transactional
    public void deleteAccount(Account account) {
        accountDao.delete(account);
    }

    public List<Account> list() {
        return accountDao.list();
    }

    public List<Account> findByPartName(String name) {
        return accountDao.findByPartName(name);
    }

    public List<Account> findByPartName(String name, int cntLimit, int pageNamber){
        return accountDao.findByPartName(name, cntLimit, pageNamber);
    }

    public Long getCntByPartName(String name) {
        return accountDao.getCntByPartName(name);
    }

    public Account find(String email) {
        return accountDao.findByEmail(email);
    }

    public Account findUUID(String uuid) {
        return accountDao.findByUUID(uuid);
    }

    public Account findById(Integer id) {
        return accountDao.find(id);
    }

    @Transactional
    public void sendFriendReq(Account actionAccount, Account account) {
        accountDao.sendFriendReq(actionAccount, account);
    }

    @Transactional
    public void acceptFriend(Account actionAccount, Account account) {
        accountDao.acceptFriend(actionAccount, account);
    }

    @Transactional
    public void declineFriend(Account actionAccount, Account account) {
        accountDao.declineFriend(actionAccount, account);
    }


}
