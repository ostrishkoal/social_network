package com.getjavajob.training.web1608.ostrishkoa.checkimpl;

import com.getjavajob.training.web1608.ostrishkoa.core.Account;
import com.getjavajob.training.web1608.ostrishkoa.validation.check.Check;
import com.getjavajob.training.web1608.ostrishkoa.validation.exception.ValidationException;

public class CheckFullName implements Check<Account> {
    public void check(Account account) throws ValidationException {
        if(account.getName() == null || account.getSurname() == null ){
            return;
        }
        String name = account.getName() + " " + account.getSurname();
        if (CheckingString.checkString(name)) {
            throw new ValidationException("entered field \"name of client\" is empty.");
        }
    }
}
