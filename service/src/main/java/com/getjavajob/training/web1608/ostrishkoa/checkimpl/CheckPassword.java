package com.getjavajob.training.web1608.ostrishkoa.checkimpl;

import com.getjavajob.training.web1608.ostrishkoa.core.Account;
import com.getjavajob.training.web1608.ostrishkoa.validation.check.Check;
import com.getjavajob.training.web1608.ostrishkoa.validation.exception.ValidationException;

public class CheckPassword implements Check<Account> {
    public void check(Account account) throws ValidationException {
        if (account.getPassword() == null){
            return;
        }
        String password = account.getPassword();
        if (CheckingString.checkString(password)) {
            throw new ValidationException("entered field \"password\" is empty.");
        }
    }
}
