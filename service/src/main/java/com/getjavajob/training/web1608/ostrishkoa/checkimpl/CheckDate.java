package com.getjavajob.training.web1608.ostrishkoa.checkimpl;

import com.getjavajob.training.web1608.ostrishkoa.core.Account;
import com.getjavajob.training.web1608.ostrishkoa.validation.check.Check;
import com.getjavajob.training.web1608.ostrishkoa.validation.exception.ValidationException;

import java.util.*;

public class CheckDate implements Check<Account> {
    public static String DATE_FORMAT = "DD.MM.YYYY";

    public void check(Account account) throws ValidationException {
        if(account.getDateBirth() == null){
            return;
        }
        Date dateOfBirth = account.getDateBirth();

        Date today = new Date();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(today);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        today = cal.getTime();

        if (today.compareTo(dateOfBirth) < 0) {
            throw new ValidationException("entered field \"dateOfBirth\": \"today\" is less then dateOfBirth: " + dateOfBirth);
        }
    }
}
