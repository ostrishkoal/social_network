package com.getjavajob.training.web1608.ostrishkoa.checkimpl;

import com.getjavajob.training.web1608.ostrishkoa.core.*;
import com.getjavajob.training.web1608.ostrishkoa.validation.check.Check;
import com.getjavajob.training.web1608.ostrishkoa.validation.exception.ValidationException;

public class CheckPhones implements Check<Account> {
    public static String COUNTRY_PHONE_CODE = "+79";
    public static int LEN_PHONE = 12;

    public void check(Account account) throws ValidationException {
        if (account.getPhones() == null){
            return;
        }
        for (Phone phone : account.getPhones()) {
            String ph = phone.getNumber();
            if (CheckingString.checkString(ph)) {
                throw new ValidationException("entered field \"number of phone\" is empty.");
            }
            if (!ph.substring(0, 3).contains(COUNTRY_PHONE_CODE)) {
                throw new ValidationException("entered field \"number of phone\": \"" + ph + "\" not start with " + COUNTRY_PHONE_CODE);
            }
            if (ph.length() != LEN_PHONE) {
                throw new ValidationException("entered field \"number of phone\": \"" + ph + "\" incorrect length: " + LEN_PHONE);
            }
        }
    }
}
