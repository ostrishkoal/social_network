package com.getjavajob.training.web1608.ostrishkoa.checkimpl;

import com.getjavajob.training.web1608.ostrishkoa.core.*;
import com.getjavajob.training.web1608.ostrishkoa.core.additions.Sex;
import com.getjavajob.training.web1608.ostrishkoa.validation.check.Check;
import com.getjavajob.training.web1608.ostrishkoa.validation.exception.ValidationException;

public class CheckSex implements Check<Account> {
    public void check(Account account) throws ValidationException {
        if (account.getSex() == null){
            return;
        }
        for (Sex sex: Sex.values()) {
            if(sex.name().equals(account.getSex())){
                return;
            }
        }
        throw new ValidationException("entered field \"sex\": " + account.getSex() + " is not valid");
    }
}
