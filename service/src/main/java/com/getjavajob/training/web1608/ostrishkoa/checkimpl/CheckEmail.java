package com.getjavajob.training.web1608.ostrishkoa.checkimpl;

import com.getjavajob.training.web1608.ostrishkoa.core.Account;
import com.getjavajob.training.web1608.ostrishkoa.validation.check.Check;
import com.getjavajob.training.web1608.ostrishkoa.validation.exception.ValidationException;

public class CheckEmail implements Check<Account> {
    public static String EMAIL_CHAR = "@";

    public void check(Account account) throws ValidationException {
        if(account.getEmail() == null){
            return;
        }
        String email = account.getEmail();
        if (CheckingString.checkString(email)) {
            throw new ValidationException("entered field \"email\" is empty.");
        }
        if (!email.contains(EMAIL_CHAR)) {
            throw new ValidationException("entered field \"email\": \"" + email + "\"  not contain: " + EMAIL_CHAR);
        }
    }
}
