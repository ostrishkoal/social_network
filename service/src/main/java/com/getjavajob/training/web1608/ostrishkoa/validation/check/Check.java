package com.getjavajob.training.web1608.ostrishkoa.validation.check;

import com.getjavajob.training.web1608.ostrishkoa.validation.exception.ValidationException;

public interface Check<T> {
     void check(T p) throws ValidationException;
}
