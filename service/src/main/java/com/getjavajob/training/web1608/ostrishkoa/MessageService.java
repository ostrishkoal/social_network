package com.getjavajob.training.web1608.ostrishkoa;

import com.getjavajob.training.web1608.ostrishkoa.core.*;
import com.getjavajob.training.web1608.ostrishkoa.dao.MessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MessageService {
    @Autowired
    private MessageDao messageDao;

    public MessageService(MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    @Transactional
    public void create(Message msg) {
        messageDao.create(msg);
    }

    public Message find(Integer id) {
        return messageDao.find(id);
    }

    public List<Message> list(Account sender, Account receiver, int cnt) {
       return messageDao.list(sender, receiver, cnt);
    }
}