package com.getjavajob.training.web1608.ostrishkoa.validation;

import com.getjavajob.training.web1608.ostrishkoa.validation.check.Check;
import com.getjavajob.training.web1608.ostrishkoa.validation.exception.ValidationException;

import java.util.*;

public class Validator<T> {
    private List<Check<T>> checkList;

    public Validator() {
        checkList = new ArrayList<>();
    }

    public Validator(List<Check<T>> checkList) {
        this.checkList = checkList;
    }

    public void setCheckList(List<Check<T>> checkList) {
        this.checkList = checkList;
    }

    public void validate(T entity) throws ValidationException {
        for (Check<T> val : checkList) {
            val.check(entity);
        }
    }
}

