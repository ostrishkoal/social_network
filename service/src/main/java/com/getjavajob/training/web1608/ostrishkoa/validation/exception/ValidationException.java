package com.getjavajob.training.web1608.ostrishkoa.validation.exception;

public class ValidationException extends Exception {
    public ValidationException(String msg) {
        super(msg);
    }
}
