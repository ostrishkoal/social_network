package com.getjavajob.training.web1608.ostrishkoa.validation.exception;

public class ServiceException extends Exception {
    public ServiceException(String msg) {
        super(msg);
    }

    public ServiceException(Exception e) {
        super(e);
    }
}
