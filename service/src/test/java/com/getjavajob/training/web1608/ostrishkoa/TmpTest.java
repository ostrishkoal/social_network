package com.getjavajob.training.web1608.ostrishkoa;

import com.getjavajob.training.web1608.ostrishkoa.core.*;
import com.getjavajob.training.web1608.ostrishkoa.validation.exception.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:service-context-override.xml"})
public class TmpTest {
    @Autowired
    AccountService accountService;
    @Autowired
    GroupService groupService;
    @Autowired
    MessageService messageService;

    @Test
    public void test() throws ServiceException, ClassNotFoundException {
        Account accountOne = accountService.findById(12);
        Account accountTwo = accountService.findById(22);
       // Group gp = groupService.findById(12);
//        System.out.println(gp.getFollowers());
        System.out.println(messageService.list(accountOne, accountTwo, 2));
        //System.out.println(accountOne.getCreatedGroups());
    }
}
