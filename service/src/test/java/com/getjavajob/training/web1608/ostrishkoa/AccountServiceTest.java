package com.getjavajob.training.web1608.ostrishkoa;

import com.getjavajob.training.web1608.ostrishkoa.core.Account;
import com.getjavajob.training.web1608.ostrishkoa.dao.AccountDao;
import com.getjavajob.training.web1608.ostrishkoa.exception.DAOException;
import com.getjavajob.training.web1608.ostrishkoa.validation.exception.ServiceException;
import org.junit.*;

import static org.mockito.Mockito.*;

public class AccountServiceTest {
    private AccountDao accountDao;
    private AccountService accountService;

    @Before
    public void init() throws ServiceException, DAOException {
        accountDao = mock(AccountDao.class);
        accountService = new AccountService();
        accountService.setAccountDao(accountDao);
    }

    @Test
    public void createAccount() throws DAOException, ServiceException {
        Account account = new Account();
        doNothing().when(accountDao).create(account);
        accountService.createAccount(account);
        verify(accountDao, times(1)).create(account);
    }

    @Test(expected = ServiceException.class)
    public void createAccountException() throws DAOException, ServiceException {
        Account account = new Account();
        account.setEmail("secondmail.ru");
        account.setPassword("second");
        doNothing().when(accountDao).create(account);
        accountService.createAccount(account);
    }

    @Test
    public void updateAccount() throws DAOException, ServiceException {
        Account account = new Account();
        doNothing().when(accountDao).update(account);
        accountService.updateAccount(account);
        verify(accountDao, times(1)).update(account);
    }

    @Test
    public void deleteAccount() throws DAOException, ServiceException {
        Account account = new Account();
        doNothing().when(accountDao).delete(account);
        accountService.deleteAccount(account);
        verify(accountDao, times(1)).delete(account);
    }

//    @Test
    //    public void listFriends() throws DAOException, ServiceException {
    //        Account account = new Account();
    //        account.setEmail("secondmail.ru");
    //        account.setPassword("second");
    //        List<Account> listDB = new ArrayList<>(asList(account));
    //        when(accountDao.listFriends(account)).thenReturn(listDB);
    //        List<Account> listService = accountService.listFriends(account);
    //        assertEquals(listDB, listService);
    //    }
    //
    //    @Test
    //    public void sendFriendReq() throws DAOException, ServiceException {
    //        Account actionAccount = new Account();
    //        Account account = new Account();
    //        doNothing().when(accountDao).sendFriendReq(actionAccount, account);
    //        accountService.sendFriendReq(actionAccount, account);
    //        verify(accountDao, times(1)).sendFriendReq(actionAccount, account);
    //    }
    //
    //    @Test
    //    public void acceptFriend() throws DAOException, ServiceException {
    //        Account actionAccount = new Account();
    //        Account account = new Account();
    //        doNothing().when(accountDao).acceptFriend(actionAccount, account);
    //        accountService.acceptFriend(actionAccount, account);
    //        verify(accountDao, times(1)).acceptFriend(actionAccount, account);
    //    }
    //
    //    @Test
    //    public void declineFriend() throws DAOException, ServiceException {
    //        Account actionAccount = new Account();
    //        Account account = new Account();
    //        doNothing().when(accountDao).declineFriend(actionAccount, account);
    //        accountService.declineFriend(actionAccount, account);
    //        verify(accountDao, times(1)).declineFriend(actionAccount, account);
    //    }
    //
    //    @Test
    //    public void getRelation() throws DAOException, ServiceException {
    //        Account actionAccount = new Account();
    //        Account account = new Account();
    //        when(accountDao.getRelation(actionAccount, account)).thenReturn(Status.ACCEPTED);
    //        Status status = accountService.getRelation(actionAccount, account);
    //        assertEquals(status, Status.ACCEPTED);
    //    }
}
