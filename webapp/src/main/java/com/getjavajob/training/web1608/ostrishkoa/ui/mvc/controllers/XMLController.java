package com.getjavajob.training.web1608.ostrishkoa.ui.mvc.controllers;

import com.getjavajob.training.web1608.ostrishkoa.core.Account;
import com.getjavajob.training.web1608.ostrishkoa.ui.util.XstreamSerializer;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;

@Controller
public class XMLController {
    @RequestMapping(value = "/loadXml", method = RequestMethod.POST)
    public ModelAndView loadXml(@RequestParam("xml") MultipartFile xml) throws ServletException {
        ModelAndView modelAndView = new ModelAndView("registration");
        try {
            if (xml != null && !xml.isEmpty()) {
                String strAccount = IOUtils.toString(xml.getInputStream());
                XstreamSerializer xstream = new XstreamSerializer();
                Account account = xstream.deserialize(strAccount);
                modelAndView.addObject("account", account);
            }
        } catch (IOException | SAXException e) {
            throw new ServletException(e);
        }
        return modelAndView;
    }


    @RequestMapping(value = "/getXml", method = RequestMethod.GET)
    public void getXml(HttpSession session, HttpServletResponse response) throws ServletException {
        try (OutputStream os = response.getOutputStream()) {
            Account account = (Account) session.getAttribute("account");
            XstreamSerializer xstream = new XstreamSerializer();
            String strAccount = xstream.serialize(account);
            response.setContentType("application/xml");// MIME type of the file
            response.setHeader("Content-Disposition", "attachment; this account");// Response header

            byte[] buffer = strAccount.getBytes();
            os.write(buffer, 0, buffer.length);
        } catch (IOException | SAXException e) {
            throw new ServletException(e);
        }
    }
}
