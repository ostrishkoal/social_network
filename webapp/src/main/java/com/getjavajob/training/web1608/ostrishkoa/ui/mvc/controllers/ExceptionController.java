package com.getjavajob.training.web1608.ostrishkoa.ui.mvc.controllers;

import org.slf4j.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

//@ControllerAdvice
@Controller
public class ExceptionController {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionController.class);

    @ExceptionHandler(Exception.class)
    public String handleEmployeeNotFoundException(HttpServletRequest request, Exception ex) {
        logger.error("Requested URL=" + request.getRequestURL());
        logger.error("Exception Raised=" + ex);
        return "redirect:/errorPage";
    }

    @RequestMapping(value = "/errorPage")
    public String error() {
        return "errorPage";
    }
}
