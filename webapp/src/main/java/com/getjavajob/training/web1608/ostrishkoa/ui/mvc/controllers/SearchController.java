package com.getjavajob.training.web1608.ostrishkoa.ui.mvc.controllers;

import com.getjavajob.training.web1608.ostrishkoa.*;
import com.getjavajob.training.web1608.ostrishkoa.core.*;
import com.getjavajob.training.web1608.ostrishkoa.ui.util.Type;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Stream.concat;

@Controller
@PropertySource(value = "classpath:config.properties")
public class SearchController {
    @Value("${search.count_on_page}")
    private int countOnPage;
    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    @RequestMapping("/autocomplete")
    @ResponseBody
    public List<ResultEntity> search(@RequestParam("part") String part) {
        List<Account> accounts = accountService.findByPartName(part, countOnPage, 1);
        List<Group> groups = groupService.findByPartName(part, countOnPage, 1);
        return concat(accounts.stream().map(ResultEntity::new), groups.stream().map(ResultEntity::new)).collect(Collectors.toList());
    }

    @RequestMapping("/searchPart")
    public ModelAndView searchPart(@RequestParam("part") String part) {
        long cntAccounts = accountService.getCntByPartName(part);
        List<Account> accounts = accountService.findByPartName(part, countOnPage, 1);
        long cntGroups = groupService.getCntByPartName(part);
        List<Group> groups = groupService.findByPartName(part, countOnPage, 1);

        ModelAndView modelAndView = new ModelAndView("search");
        modelAndView.addObject("accounts", accounts);
        modelAndView.addObject("cntAccounts", cntAccounts);
        modelAndView.addObject("groups", groups);
        modelAndView.addObject("cntGroups", cntGroups);
        modelAndView.addObject("cnt", countOnPage);
        return modelAndView;
    }

    @RequestMapping("/search/account")
    @ResponseBody
    public List<ResultEntity> searchAccounts(@RequestParam("part") String part, @RequestParam("num") int num) {
        List<Account> accounts = accountService.findByPartName(part, countOnPage, num);
        return accounts.stream().map(ResultEntity::new).collect(Collectors.toList());
    }

    @RequestMapping("/search/group")
    @ResponseBody
    public List<ResultEntity> searchGroups(@RequestParam("part") String part, @RequestParam("num") int num) {
        List<Group> groups = groupService.findByPartName(part, countOnPage, num);
        return groups.stream().map(ResultEntity::new).collect(Collectors.toList());
    }

    private class ResultEntity {
        private int id;
        private Type type;
        private String desc;
        private String photoString;

        ResultEntity(Object obj) {
            if (obj instanceof Account) {
                Account account = (Account) obj;
                id = account.getId();
                type = Type.ACCOUNT;
                desc = account.getName() + " " + account.getSurname();
                photoString = account.getPhotoString();
            }
            if (obj instanceof Group) {
                Group group = (Group) obj;
                id = group.getId();
                type = Type.GROUP;
                desc = group.getName();
                photoString = group.getPhotoString();
            }
        }

        public String getPhotoString() {
            return photoString;
        }

        public void setPhotoString(String photoString) {
            this.photoString = photoString;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

}
