package com.getjavajob.training.web1608.ostrishkoa.ui.mvc.controllers;

import com.getjavajob.training.web1608.ostrishkoa.AccountService;
import com.getjavajob.training.web1608.ostrishkoa.core.Account;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.security.Principal;

@Controller
public class LogInOutController {
    private static final Logger logger = LoggerFactory.getLogger(LogInOutController.class);
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/")
    public String start() {
        return "redirect:/home";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "index";
    }

    @RequestMapping(value = "/home")
    public String login(Principal principal, HttpSession session) throws ServletException {
        Account account = accountService.find(principal.getName());
        session.setAttribute("account", account);
        logger.info("ID: " + account.getId() + " log in");
        return "redirect:account/" + account.getId();
    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout=true";
    }
}
