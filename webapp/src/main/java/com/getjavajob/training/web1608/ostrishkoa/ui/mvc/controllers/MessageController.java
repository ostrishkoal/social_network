package com.getjavajob.training.web1608.ostrishkoa.ui.mvc.controllers;

import com.getjavajob.training.web1608.ostrishkoa.*;
import com.getjavajob.training.web1608.ostrishkoa.core.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.PropertySource;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.List;

@Controller
@PropertySource(value = "classpath:config.properties")
public class MessageController {
    @Value("${chat.cnt_msgs}")
    private int cntLastMsgs;

    @Autowired
    private MessageService messageService;
    @Autowired
    private AccountService accountService;

    @RequestMapping("account/chat/{receiverId}")
    public ModelAndView viewChat(@PathVariable("receiverId") int receiverId, HttpSession session) {
        Account sender = (Account) session.getAttribute("account");
        Account receiver = accountService.findById(receiverId);
        List<Message> messages = messageService.list(sender, receiver, cntLastMsgs);

        ModelAndView modelAndView = new ModelAndView("chat");
        modelAndView.addObject("messages", messages);
        modelAndView.addObject("receiver", receiver);
        return modelAndView;
    }


    @MessageMapping("/message/{receiverId}")
    @SendTo("/topic/message/{receiverId}")
    public String handlerMsg(Principal principal, @DestinationVariable String receiverId, org.springframework.messaging.Message msg) {
        Account sender = accountService.find(principal.getName());
        Account receiver = accountService.findById(Integer.valueOf(receiverId));
        String text = new String((byte[]) msg.getPayload());
        Message message = new Message(sender, receiver, text);
        messageService.create(message);
        return text;
    }
}
