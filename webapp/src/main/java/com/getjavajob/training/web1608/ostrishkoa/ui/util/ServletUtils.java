package com.getjavajob.training.web1608.ostrishkoa.ui.util;

import javax.servlet.http.*;
import java.security.*;

public class ServletUtils {
    public static final String LOGIN_COOKIE = "login";
    public static final String ERROR_LOGIN = "INCORRECT EMAIL OR PASSWORD";
    public static final String SESSION_ID = "JSESSIONID";
    public static final int SESSION_TIMEOUT = 10 * 60;
    public static final int COOKIE_TIMEOUT = 60 * 60 * 24 * 30;

    public static String getCookieValue(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (name.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    public static void addCookie(HttpServletResponse response, String name, String value, int maxAge) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        cookie.setMaxAge(maxAge);
        response.addCookie(cookie);
    }

    public static void removeCookie(HttpServletResponse response, String name) {
        addCookie(response, name, null, 0);
    }

    public static String getSHAHash(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes());
        byte byteData[] = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
