package com.getjavajob.training.web1608.ostrishkoa.ui.mvc.controllers;

import com.getjavajob.training.web1608.ostrishkoa.*;
import com.getjavajob.training.web1608.ostrishkoa.core.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@Controller
@SessionAttributes("account")
@PropertySource(value = "classpath:config.properties")
public class GroupController {
    @Value("${search.count_on_page}")
    private int countOnPage;
    @Autowired
    private GroupService groupService;
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/group/{id}", method = RequestMethod.GET)
    public ModelAndView getGroup(@PathVariable("id") int id) {
        ModelAndView modelAndView = new ModelAndView("groupPage");
        Group group = groupService.findById(id);
        modelAndView.addObject("group", group);
        return modelAndView;
    }

    @RequestMapping(value = "/editGroup")
    public ModelAndView createAccount(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("groupEdit");
        Group group = (Group) session.getAttribute("group");
        if (group == null) {
            System.out.println(group);
            modelAndView.addObject("group", new Group());
        } else {
            modelAndView.addObject("group", group);
        }
        return modelAndView;
    }

    @RequestMapping(value = "/saveGroup", method = RequestMethod.POST)
    public String saveAccount(@ModelAttribute("group") Group group, @RequestParam(value = "file", required = false) MultipartFile file, BindingResult result, HttpSession session) throws ServletException {
        if (result.hasErrors()) {
            System.out.println(Arrays.asList(result.getAllErrors().toArray()));
            return "errorPage";
        }
        Group sessionGroup = (Group) session.getAttribute("group");

        if (sessionGroup != null) {
            group.setId(sessionGroup.getId());
            group.setCreator(sessionGroup.getCreator());
            if (sessionGroup.getPhoto() != null && sessionGroup.getPhoto().length > 0) {
                group.setPhoto(sessionGroup.getPhoto());
            }
        }
        try {
            if (!file.isEmpty() && file.getInputStream().available() != 0) {
                group.setPhoto(file.getBytes());
            }
        } catch (IOException e) {
            throw new ServletException(e);
        }

        if (group.getId() == null) {
            Account creator = (Account) session.getAttribute("account");
            group.setCreator(creator);
            groupService.createGroup(group);
        } else {
            groupService.updateGroup(group);
            session.removeAttribute("group");
        }
        return "redirect:/group/" + group.getId();
    }


    @RequestMapping(value = "/account/{id}/groups", method = RequestMethod.GET)
    public ModelAndView getFriends(@PathVariable("id") int id) {
        Account account = accountService.findById(id);
        Set<Group> reqsFrom = account.getRequestedGroups();
        Set<Group> groups = account.getGroups();
        groups.addAll(account.getCreatedGroups());

        ModelAndView modelAndView = new ModelAndView("groups");
        modelAndView.addObject("groups", groups);
        modelAndView.addObject("cntGroups", groups.size());
        modelAndView.addObject("reqsFrom", reqsFrom);
        modelAndView.addObject("cntReqsFrom", reqsFrom.size()); //todo
        modelAndView.addObject("cnt", countOnPage);
        return modelAndView;
    }

    @RequestMapping(value = "/gp/{id}/sendGroupReq", method = RequestMethod.GET)
    @ResponseBody
    public void sendGroupReq(@PathVariable("id") int id, @RequestParam(value = "accountId") int accountId, HttpSession session) {
        Group group = groupService.findById(id);
        Account actionAccount = accountService.findById(accountId);
        groupService.sendGroupReq(actionAccount, group);
    }

    @RequestMapping(value = "/gp/{id}/acceptGroupReq", method = RequestMethod.GET)
    @ResponseBody
    public void acceptGroupReq(@PathVariable("id") int id, @RequestParam(value = "accountId") int accountId, HttpSession session) {
        Group group = groupService.findById(id);
        Account actionAccount = accountService.findById(accountId);
        groupService.acceptMember(actionAccount, group);
    }

    @RequestMapping(value = "/gp/{id}/declineGroupReq", method = RequestMethod.GET)
    @ResponseBody
    public void declineGroupReq(@PathVariable("id") int id, @RequestParam(value = "accountId") int accountId, HttpSession session) {
        Group group = groupService.findById(id);
        Account actionAccount = accountService.findById(accountId);
        groupService.declineMember(actionAccount, group);
    }

    @RequestMapping(value = "/gp/{id}/members", method = RequestMethod.GET)
    public ModelAndView listMembers(@PathVariable("id") int id, HttpSession session) {
        Group group = groupService.findById(id);
        Account actionAccount = (Account) session.getAttribute("account");

        ModelAndView modelAndView = new ModelAndView("groupMembers");
        Set<Account> members = group.getMembers();
        members.add(group.getCreator());
        Set<Account> followers = group.getFollowers();
        modelAndView.addObject("members", members);
        modelAndView.addObject("followers", followers);
        modelAndView.addObject("group", group);
        return modelAndView;
    }
}
