package com.getjavajob.training.web1608.ostrishkoa.ui.mvc.controllers;

import com.getjavajob.training.web1608.ostrishkoa.AccountService;
import com.getjavajob.training.web1608.ostrishkoa.core.*;
import com.getjavajob.training.web1608.ostrishkoa.validation.exception.ServiceException;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.getjavajob.training.web1608.ostrishkoa.ui.util.ServletUtils.getSHAHash;

@Controller
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/account/{id}/friends", method = RequestMethod.GET)
    public ModelAndView getFriends(@PathVariable("id") int id) {
        ModelAndView modelAndView = new ModelAndView("friends");
        //List<Account> friends = accountService.listFriends(accountService.findById(id));
        //modelAndView.addObject("friends", friends);
        return modelAndView;
    }

    @RequestMapping(value = "/account/{id}/sendFriendReq", method = RequestMethod.GET)
    @ResponseBody
    public void sendFriendReq(@PathVariable("id") int id, HttpSession session) {
        Account account = accountService.findById(id);
        Account actionAccount = (Account) session.getAttribute("account");
        accountService.sendFriendReq(actionAccount, account);
    }

    @RequestMapping(value = "/account/{id}/declineFriendReq", method = RequestMethod.GET)
    @ResponseBody
    public void declineFriendReq(@PathVariable("id") int id, HttpSession session) {
        Account account = accountService.findById(id);
        Account actionAccount = (Account) session.getAttribute("account");
        accountService.declineFriend(actionAccount, account);
    }

    @RequestMapping(value = "/account/{id}/acceptFriend", method = RequestMethod.GET)
    @ResponseBody
    public void acceptFriend(@PathVariable("id") int id, HttpSession session) {
        Account account = accountService.findById(id);
        Account actionAccount = (Account) session.getAttribute("account");
        accountService.acceptFriend(actionAccount, account);
    }

    @RequestMapping(value = "/account/{id}/removeFriend", method = RequestMethod.GET)
    @ResponseBody
    public void removeFriend(@PathVariable("id") int id, HttpSession session) {
        Account account = accountService.findById(id);
        Account actionAccount = (Account) session.getAttribute("account");
        accountService.declineFriend(actionAccount, account);
    }

    @RequestMapping(value = "/account/{id}", method = RequestMethod.GET)
    public ModelAndView getAccount(@PathVariable("id") int id) {
        ModelAndView modelAndView = new ModelAndView("accountPage");
        Account thisAccount = accountService.findById(id);
        modelAndView.addObject("thisAccount", thisAccount);
        return modelAndView;
    }

    @RequestMapping(value = "/edit")
    public ModelAndView createAccount(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("registration");
        Account account = (Account) session.getAttribute("account");
        System.out.println("edit" + account);
        if (account == null) {
            modelAndView.addObject("account", new Account());
        } else {
            modelAndView.addObject("account", account);
        }
        return modelAndView;
    }

    @RequestMapping(value = "/saveAccount", method = RequestMethod.POST)
    public String saveAccount(@ModelAttribute("account") Account account, @RequestParam(value = "file", required = false) MultipartFile file, HttpSession session) throws ServiceException {
        Account sessionAccount = (Account) session.getAttribute("account");
        try {
            if (sessionAccount != null) {
                account.setId(sessionAccount.getId());
            }
            if (!file.isEmpty() && file.getInputStream().available() != 0) {
                account.setPhoto(file.getBytes());
            } else if (sessionAccount != null && sessionAccount.getPhoto() != null && sessionAccount.getPhoto().length > 0) {
                account.setPhoto(sessionAccount.getPhoto());
            }

            List<Phone> phones = new ArrayList<>(); //todo
            for (Phone phone : account.getPhones()) {
                if (phone.getNumber() != null && phone.getNumber().length() > 0) {
                    phones.add(phone);
                }
            }
            account.setPhones(phones);

            account.setPassword(getSHAHash(account.getPassword()));

            if (account.getId() == null) {
                accountService.createAccount(account);
                logger.info("ID: " + account.getId() + " create");
                return "redirect:/login";
            } else {
                if (account.getPassword().equals(sessionAccount.getPassword())) {//todo registration field pass
                    accountService.updateAccount(account);
                    session.setAttribute("account", account);
                    logger.info("ID: " + account.getId() + " update");
                    return "redirect:/account/" + account.getId();
                }
            }
            return "redirect:/errorPage";
        } catch (ServiceException | NoSuchAlgorithmException | IOException e) {
            throw new ServiceException(e);
        }
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, "dateBirth", new CustomDateEditor(dateFormat, true));
    }
}
