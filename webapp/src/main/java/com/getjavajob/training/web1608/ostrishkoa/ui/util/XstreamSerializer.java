package com.getjavajob.training.web1608.ostrishkoa.ui.util;

import com.getjavajob.training.web1608.ostrishkoa.core.*;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.xml.sax.SAXException;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Date;

public class XstreamSerializer {
    private XStream xstream;

    public XstreamSerializer() throws SAXException {
        this.xstream = new XStream(new DomDriver());
        xstream.addDefaultImplementation(java.sql.Date.class, Date.class);
        xstream.processAnnotations(Account.class);
        xstream.processAnnotations(Phone.class);
    }

    public Account deserialize(String fromXml) {
        try {
            return (Account) xstream.fromXML(fromXml);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String serialize(Account account) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            Writer writer = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            xstream.toXML(account, writer);
            String accountXml = outputStream.toString("UTF-8");
            return accountXml;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
