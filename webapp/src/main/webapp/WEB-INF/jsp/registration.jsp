<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <spring:url value="/resources/bootstrap/css/bootstrap.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8/jquery.validate.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <spring:url value="/resources/js/datepicker.js" var="datepicker"/>
    <script src="${datepicker}"></script>
    <spring:url value="/resources/js/phones.js" var="phones"/>
    <script src="${phones}"></script>
    <spring:url value="/resources/js/bootbox.min.js" var="bootbox"/>
    <script src="${bootbox}"></script>
    <spring:url value="/resources/js/validation.js" var="validation"/>
    <script src="${validation}"></script>
    <spring:url value="/resources/bootstrap/js/bootstrap.js" var="bootstrapJs"/>
    <script src="${bootstrapJs}"></script>

    <title></title>
</head>
<body>
<form:form id="myform" class="form-horizontal" modelAttribute="account"
           action="${pageContext.request.contextPath}/saveAccount"
           method="POST"
           enctype="multipart/form-data">

    <fieldset>
        <!-- Form Name -->
        <legend></legend>

        <div class="form-group">
            <label class="col-md-4 control-label" for="date">Date of Birth</label>
            <div class="col-md-4">
                <fmt:formatDate var="fmtDate" value="${account.dateBirth}" pattern="dd-mm-yyyy"/>
                <form:input id="date" class="form-control" type="text" path="dateBirth" value="${fmtDate}"/><br>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="fn">Name</label>
            <div class="col-md-4">
                <form:input id="fn" path="name" type="text" placeholder="name" class="form-control input-md" required=""
                            value="${account.name}"/>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="ln">Surame</label>
            <div class="col-md-4">
                <form:input id="ln" path="surname" type="text" placeholder="surname" class="form-control input-md"
                            value="${account.surname}"/>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email</label>
            <div class="col-md-4">
                <form:input id="email" path="email" type="text" placeholder="email" class="form-control input-md"
                            value="${account.email}"/>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-4">
                <form:input id="password" path="password" value="" type="password" placeholder="password"
                            class="form-control input-md" required=""/>
            </div>
        </div>

        <!-- This is the dynamic field we want to manipulate -->
        <div class="form-group">
            <label class="col-md-4 control-label">Phones</label>
            <div class="col-md-4">

                <c:forEach items="${account.phones}" var="element" varStatus="loop">
                    <div class="form-group multiple-form-group input-group">
                        <form:select class="selectpicker form-control type" path="phones[${loop.index}].type"
                                     value="${element.type}">
                            <form:option value="WORK"/>
                            <form:option value="HOME"/>
                        </form:select>
                        <span class="input-group-addon"></span>
                        <form:input type="text" class="form-control number" path="phones[${loop.index}].number"
                                    value="${element.number}"/>
                        <span class="input-group-btn">
							<button type="button" class="btn btn-danger btn-remove">-</button>
						</span>
                    </div>
                </c:forEach>

                <div class="form-group multiple-form-group input-group">
                    <c:set var="index" value="${fn:length(account.phones)}"/>
                    <select name="phones[${index}].type" value="WORK" class="selectpicker form-control type">
                        <option value="WORK">WORK</option>
                        <option value="HOME">HOME</option>
                    </select>
                    <span class="input-group-addon"></span>
                    <input type="text" name="phones[${index}].number" class="form-control number" disabled="disabled"/>
                    <span class="input-group-btn">
						<button type="button" class="btn btn-primary btn-add">+</button>
					</span>
                </div>

            </div>
        </div>


        <!--         <div class="form-group">
                    <label class="col-md-4 control-label">Gender</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="radio-inline">
                                    <input type="radio" id="femaleRadio" value="F">Female
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="radio-inline">
                                    <input type="radio" id="maleRadio" value="M">Male
                                </label>
                            </div>
                        </div>
                    </div>
                </div> -->

        <div class="form-group">
            <label class="col-md-4 control-label" for="uploadimage">Upload Image: </label>
            <div class="col-md-4">
                <input id="uploadimage" name="file" type="file">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="ok"></label>
            <div class="col-md-4">
                <button id="ok" type=button name="Ok" value="Ok" class="btn btn-block btn-primary">SUBMIT</button>
                    <%--<input type="submit" style="visibility: hidden;" />--%>
            </div>
        </div>
    </fieldset>
</form:form>
<form:form id="formXML" name="formXML" class="form-horizontal"
           action="${pageContext.request.contextPath}/loadXml"
           method="POST"
           enctype="multipart/form-data">
    <fieldset>
        <div class="form-group">
            <label class="col-md-4 control-label" for="uploadXml">Upload XML: </label>
            <div class="col-md-4">
                <input id="uploadXml" name="xml" type="file">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="submitXml"></label>
            <div class="col-md-4">
                <button id="submitXml" class="btn btn-block btn-primary">IMPORT</button>
            </div>
        </div>
    </fieldset>
</form:form>
<script>
    setTimeout(
            function () {
                $(':password').val('');
            },
            3 //1,000 milliseconds = 1 second
    );
</script>

</body>
</html>
