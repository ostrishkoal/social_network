<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <c:import url="/WEB-INF/jsp/header.jsp"/>

    <spring:url value="/resources/css/chat.css" var="chatCss"/>
    <link href="${chatCss}" rel="stylesheet"/>

    <spring:url value="/resources/js/sockjs.min.js" var="sockJs"/>
    <script src="${sockJs}"></script>
    <spring:url value="/resources/js/stomp.min.js" var="stompJs"/>
    <script src="${stompJs}"></script>
    <spring:url value="/resources/js/chat.js" var="chat"/>
    <script src="${chat}"></script>
</head>
<body>
<div class="chat_window">
    <div class="top_menu">
        <div class="buttons">
            <div class="button close"></div>
            <div class="button minimize"></div>
            <div class="button maximize"></div>
        </div>
        <div class="title">Chat</div>
    </div>
    <ul class="messages">
        <c:forEach items="${messages}" var="element">
            <c:set var="side" value="right"/>
            <c:if test="${element.accountFrom.equals(receiver)}">
                <c:set var="side" value="left"/>
            </c:if>
            <li class="message ${side} appeared">
                <div class="avatar"></div>
                <div class="text_wrapper">
                    <div class="text">${element.message}</div>
                </div>
            </li>
        </c:forEach>
    </ul>
    <div class="bottom_wrapper clearfix">
        <div class="message_input_wrapper">
            <input class="message_input" placeholder="Type your message here..."/></div>
        <div class="send_message">
            <div class="icon"></div>
            <div class="text">Send</div>
        </div>
    </div>
</div>
<div class="message_template">
    <li class="message">
        <div class="avatar"></div>
        <div class="text_wrapper">
            <div class="text"></div>
        </div>
    </li>
</div>
</body>
<script>
    window.receiverId = ${receiver.id};
    window.senderId = ${account.id};
</script>
</html>