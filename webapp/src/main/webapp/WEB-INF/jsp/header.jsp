<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <spring:url value="/resources/bootstrap/css/bootstrap.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <%--<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>--%>
    <spring:url value="/resources/bootstrap/js/bootstrap.js" var="bootstrapJs"/>
    <script src="${bootstrapJs}"></script>
    <spring:url value="/resources/js/search.js" var="search"/>
    <script src="${search}"></script>
</head>
<body>
<nav class="navbar navbar-inverse" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Social network</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li><a href="${pageContext.request.contextPath}/home">Home</a></li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">More information <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="${pageContext.request.contextPath}/account/${sessionScope.account.id}/friends">Friends</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="${pageContext.request.contextPath}/account/${sessionScope.account.id}/groups">Groups</a>
                    </li>
                    <li><a href="${pageContext.request.contextPath}/editGroup">Create Group</a></li>
                </ul>
            </li>

        </ul>

        <div class="col-sm-3 col-md-3">
            <form class="navbar-form" role="search" method="get" action="${pageContext.request.contextPath}/searchPart">
                <div class="input-group">
                    <input id="searchEntity" name="part" type="text" class="form-control" placeholder="Search" >
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit" >
                            <i class="glyphicon glyphicon-ok"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <!-- <li><a href="#">Link</a></li> -->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/edit">Edit</a></li>
                    <li><a href="${pageContext.request.contextPath}/getXml">Download XML</a></li>
                    <li class="divider"></li>
                    <li><a href="${pageContext.request.contextPath}/logout">Log out</a></li>
                </ul>
            </li>
        </ul>

    </div><!-- /.navbar-collapse -->
</nav>
<script>
    window.ctx = "${pageContext.request.contextPath}";
</script>
</body>
</html>