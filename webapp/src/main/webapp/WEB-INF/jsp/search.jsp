<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <c:import url="/WEB-INF/jsp/header.jsp"/>

    <spring:url value="/resources/css/list.css" var="listCss"/>
    <link href="${listCss}" rel="stylesheet"/>

    <spring:url value="/resources/img/avatar1.jpg" var="imgUrl"/>
</head>
<body>
<div class="container">
    <div class="row ng-scope">
        <div class="col-md-3 col-md-push-9">
            <h4>Results <span class="fw-semi-bold">Filtering</span></h4>
            <p class="text-muted fs-mini">Listed content is categorized by the following groups:</p>
            <ul class="nav nav-pills nav-stacked search-result-categories mt">
                <li>
                    <a id="btn-friends">Accounts <span class="badge">${cntAccounts}</span></a>
                </li>
                <li>
                    <a id="btn-groups">Groups <span class="badge">${cntGroups}</span></a>
                </li>
            </ul>
        </div>
        <div class="col-md-9 col-md-pull-3 result-list">
            <p class="search-results-count accounts">About ${cntAccounts} accounts</p>
            <p class="search-results-count groups" style="display: none;">About ${cntGroups} groups</p>

            <div class="accounts accounts-section">
                <c:forEach items="${accounts}" var="element">
                    <section class="search-result-item">
                        <a class="image-link" href="#">
                            <c:set var="url" value="imgUrl"/>
                            <c:if test="${element.photoString != null}">
                                <c:set var="url" value="data:image/jpg;base64,${element.photoString}"/>
                            </c:if>
                            <img class="image" src="${url}"/>
                        </a>
                        <div class="search-result-item-body">
                            <div class="row">
                                <div class="col-sm-9">
                                    <h4 class="search-result-item-heading">
                                        <a href="#">${element.name} ${element.surname}</a>
                                    </h4>
                                    <p class="info">profile account</p>
                                        <%--<p class="description">blabla</p>--%>
                                </div>
                                <div class="col-sm-3 text-align-center">
                                        <%--<p class="value3 mt-sm">#</p>--%>
                                        <%--<p class="fs-mini text-muted">##</p>--%>
                                    <a class="btn btn-primary btn-info btn-sm view-profile" href="${pageContext.request.contextPath}/account/${element.id}">View profile</a>
                                </div>
                            </div>
                        </div>
                    </section>
                </c:forEach>
            </div>

            <div class="groups groups-section" style="display: none;">
                <c:forEach items="${groups}" var="element">
                    <section class="search-result-item" >
                        <a class="image-link" href="#">
                            <c:set var="url" value="imgUrl"/>
                            <c:if test="${element.photoString != null}">
                                <c:set var="url" value="data:image/jpg;base64,${element.photoString}"/>
                            </c:if>
                            <img class="image" src="${url}"/>
                        </a>
                        <div class="search-result-item-body">
                            <div class="row">
                                <div class="col-sm-9">
                                    <h4 class="search-result-item-heading">
                                        <a href="#">${element.name}</a>
                                    </h4>
                                    <p class="info">profile group</p>
                                </div>
                                <div class="col-sm-3 text-align-center">
                                    <a class="btn btn-primary btn-info btn-sm view-profile" href="${pageContext.request.contextPath}/group/${element.id}">View profile</a>
                                </div>
                            </div>
                        </div>
                    </section>
                </c:forEach>
            </div>

            <div class="text-align-center accounts">
                <ul class="pagination pagination-sm">
                    <c:set var="cntAccountPages"
                           value='${cntAccounts/cnt + ( 1 - (cntAccounts/cnt%1)) % 1}'
                    />
                    <c:forEach begin="1" end="${cntAccountPages}" varStatus="loop">
                        <li>
                            <a class="account-page">${loop.index}</a>
                        </li>
                    </c:forEach>
                </ul>
            </div>

            <div class="text-align-center groups" style="display: none;">
                <ul class="pagination pagination-sm">
                    <c:set var="cntGroupPages"
                           value='${cntGroups/cnt + ( 1 - (cntGroups/cnt%1)) % 1}'
                    />
                    <c:forEach begin="1" end="${cntGroupPages}" varStatus="loop">
                        <li>
                            <a class="group-page">${loop.index}</a>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    window.part = "${param.part}";
    window.imgUrl = "${imgUrl}";
</script>
</body>
</html>