<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">

    <spring:url value="/resources/bootstrap/css/bootstrap.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8/jquery.validate.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <spring:url value="/resources/bootstrap/js/bootstrap.js" var="bootstrapJs"/>
    <script src="${bootstrapJs}"></script>

    <title></title>
</head>
<body>
<form:form id="myform" name="myform" class="form-horizontal" modelAttribute="group"
           action="${pageContext.request.contextPath}/saveGroup"
           method="POST"
           enctype="multipart/form-data">
    <div id="errorAll" class="errorAll"></div>
    <fieldset>
        <!-- Form Name -->
        <legend></legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="fn">Name</label>
            <div class="col-md-4">
                <form:input id="fn" path="name" type="text" placeholder="name" class="form-control input-md" required=""
                            value="${group.name}"/>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="ln">Desc</label>
            <div class="col-md-4">
                <form:input id="ln" path="description" type="text" placeholder="description" class="form-control input-md"
                            required="" value="${group.description}"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="uploadimage">Upload Image: </label>
            <div class="col-md-4">
                <input id="uploadimage" name="file" type="file">
            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="submit"></label>
            <div class="col-md-4">
                <button id="submit" name="Ok" value="Ok" class="btn btn-primary">SUBMIT</button>
            </div>
        </div>

    </fieldset>
</form:form>

</body>
</html>
