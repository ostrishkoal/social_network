<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <c:import url="/WEB-INF/jsp/header.jsp"/>
</head>
<body>
<br>
<p>Accounts</p>
<c:forEach var="account" items="${friends}">
    <img style="width:100px" src="data:image/jpg;base64,${account.photoString}"/>
    <p>
        <a href="${pageContext.request.contextPath}/account/${account.id}">
                ${account.surname} ${account.name}
        </a>
    </p>
</c:forEach>
</body>
</html>