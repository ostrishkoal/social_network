<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <c:import url="/WEB-INF/jsp/header.jsp"/>

    <spring:url value="/resources/js/groupPage.js" var="groupPage"/>
    <script src="${groupPage}"></script>

    <spring:url value="/resources/img/avatar1.jpg" var="imgUrl"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-7 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Group Profile</h4></div>
                <div class="panel-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="col-sm-6">
                                <div align="center">
                                    <c:if test="${group.photoString != null}">
                                        <c:set var="imgUrl" value="data:image/jpg;base64,${group.photoString}"/>
                                    </c:if>
                                    <img src="${imgUrl}" class="img-circle img-responsive">
                                </div>
                                <br>
                                <!-- /input-account -->
                            </div>
                            <div class="col-sm-6">
                                <h4 style="color:#00b1b1;">${group.name}</h4></span>
                                <span>
									<c:if test="${group.creator.id == account.id}">
                                        <c:set var="group" scope="session" value="${group}"/>
                                        <p>
											<a href="${pageContext.request.contextPath}/editGroup">#edit</a>
										</p>
                                    </c:if>
								</span>
                            </div>
                            <div class="clearfix"></div>
                            <hr style="margin:5px 0 5px 0;">


                            <div class="col-sm-5 col-xs-6 tital ">Name:</div>
                            <div class="col-sm-7 col-xs-6 ">${group.name}</div>
                            <div class="clearfix"></div>
                            <div class="bot-border"></div>

                            <div class="col-sm-5 col-xs-6 tital ">Desc:</div>
                            <div class="col-sm-7"> ${group.description}</div>
                            <div class="clearfix"></div>
                            <div class="bot-border"></div>

                            <c:if test="${sessionScope.account.id !=  group.creator.id}">
                                <c:set var="relation" value="${group.getRelationStatus(sessionScope.account)}"/>
                                <c:if test="${relation == 'DEFAULT'}">
                                    <button type="button" class="btn btn-sm btn-block btn-req">Sent request</button>
                                </c:if>
                                <c:if test="${relation == 'ACCEPTED'}">
                                    <button type="button" class="btn btn-sm btn-block btn-req">Unfollow</button>
                                </c:if>
                                <c:if test="${relation == 'PENDING'}">
                                    <button type="button" class="btn btn-sm btn-block btn-req">Decline request</button>
                                </c:if>
                            </c:if>
                            <a href="${pageContext.request.contextPath}/gp/${group.id}/members">
                                <button id="btn-members" type="button" class="btn btn-sm btn-success btn-block">
                                    Members
                                </button>
                            </a>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.groupId = "${group.id}";
    window.accountId = "${account.id}";
</script>
</body>
</html>



