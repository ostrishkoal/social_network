<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <spring:url value="/resources/bootstrap/css/bootstrap.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>
    <spring:url value="/resources/font-awesome/css/font-awesome.css" var="fontAwesomeCss"/>
    <link href="${fontAwesomeCss}" rel="stylesheet"/>

    <spring:url value="/resources/bootstrap/js/bootstrap.js" var="bootstrapJs"/>
    <script src="${bootstrapJs}"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>

<body>
<h2>${sessionScope.error}</h2>
<div class="container" style="margin-top:50px">
    <div class="col-md-4 ">
        <div class="panel panel-default">
            <div class="panel-body">
                <form role="form" action="${pageContext.request.contextPath}/j_spring_security_check" method="post">

                    <c:if test="${not empty param.error}">
                        <div class="error">Invalid login or password</div>
                    </c:if>
                    <c:if test="${not empty param.logout}">
                        <span class="msg">Success logout</span>
                    </c:if>

                    <div class="form-group">
                        <label for="exampleInputEmail1" class="sr-only">Email address</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                                        placeholder="Enter username or email"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1" class="sr-only">Password</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                                        placeholder="Password"/>
                        </div>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"/> Stay signed in
                        </label>
                    </div>

                    <button type="submit" class="btn btn-success btn-sm" name="Log in" value="Log in">Sign in</button>
                    <a href="${pageContext.request.contextPath}/edit">Create account</a>
                    <%--<input type="hidden" name="${_csrf.parameterName}"   value="${_csrf.token}" />--%>

                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>