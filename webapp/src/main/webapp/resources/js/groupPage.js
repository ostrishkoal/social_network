$(document).ready(function () {

    $('.btn-decline').on('click', function () {
        $(".btn-accept" ).remove();
    });

    $('.btn-accept').on('click', function () {
        $(".btn-decline" ).remove();
    });

    $('.btn-req').on('click', function () {
        var $elem = $(".btn-req");

        if ($elem.text().trim() === 'Decline request' || $elem.text().trim() === 'Unfollow') {
            $.ajax({
                url: window.ctx + "/gp/" + window.groupId + "/declineGroupReq?accountId="+window.accountId,
                success: function () {
                    $elem.text('Sent request');
                }
            });
        } else {
            $.ajax({
                url: window.ctx + "/gp/" + window.groupId + "/sendGroupReq?accountId="+window.accountId,
                success: function () {
                    $elem.text('Decline request');
                }
            });
        }
    });
})