$(document).ready(function () {

        var toggle = function (on, off) {
            on.show("slow");
            var display = off.css("display");
            if (display != "none") {
                off.attr("style", "display:none");
            }
        }
        $('#btn-members').on('click', function () {
            toggle($(".members"), $(".followers"))
        });
        $('#btn-followers').on('click', function () {
            toggle($(".followers"), $(".members"))
        });
        //-----------------------------------------------

    $('.btn-req').on('click', function () {
        var $elem = $(this);
        var sectionItem = $elem.closest(".search-result-item");
        var ahref = sectionItem.find('.view-profile').attr('href');
        var accountId = ahref.substring(1 + ahref.lastIndexOf("/"), ahref.length);

        if ($elem.text().trim() === 'Decline' || $elem.text().trim() === 'Remove') {
            $.ajax({
                url: window.ctx + "/gp/" + window.groupId + "/declineGroupReq?accountId=" + accountId,
                success: function () {
                    sectionItem.remove();
                }
            });
        } else {
            $.ajax({
                url: window.ctx + "/gp/" + window.groupId + "/acceptGroupReq?accountId=" + accountId,
                success: function () {
                    var sectionItemClone = sectionItem.clone();
                    sectionItemClone.find('.btn-decline').text("Remove");
                    sectionItemClone.find('.btn-accept').remove();
                    $("#members").prepend(sectionItemClone);
                    sectionItem.remove();

                }
            });
        }
    });

    }
)