$(document).ready(function () {

    $('.btn-decline').on('click', function () {
        $(".btn-accept" ).remove();
    });

    $('.btn-accept').on('click', function () {
        $(".btn-decline" ).remove();
    });

    $('.btn-req').on('click', function () {
        var $elem = $(".btn-req");

        if ($elem.text().trim() === 'Decline friend request') {
            $.ajax({
                url: window.ctx + "/account/" + window.id + "/declineFriendReq",
                success: function () {
                    $elem.text('Add as a friend');
                }
            });
        } else if ($elem.text().trim() === 'Remove friend') {
            $.ajax({
                url: window.ctx + "/account/" + window.id + "/removeFriend",
                success: function () {
                    $elem.text('Add as a friend');
                }
            });
        } else if ($elem.text().trim() === 'Accept friend request') {
            $.ajax({
                url: window.ctx + "/account/" + window.id + "/acceptFriend",
                success: function () {
                    $elem.text('Remove friend');
                }
            });
        } else {
            $.ajax({
                url: window.ctx + "/account/" + window.id + "/sendFriendReq",
                success: function () {
                    $elem.text('Decline friend request');
                }
            });
        }
    });
})