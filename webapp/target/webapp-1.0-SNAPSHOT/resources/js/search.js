$(document).ready(function () {
    $(function () {
        $("#searchEntity").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: window.ctx + "/autocomplete",
                    method: "POST",
                    data: {
                        part: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (entity, i) {
                            return {
                                // value: entity.desc,
                                label: entity.desc,
                                id: entity.id,
                                type: entity.type.toLowerCase()
                            };
                        }));
                    }
                });
            },
            minLength: 1,
            select: function (event, entity) {
                location.href = window.ctx + "/" + entity.item.type + "/" + entity.item.id;
            }
        });
    });
});

$(document).ready(function () {
    //-----------------------------------------------
    var toggle = function (on, off) {
        on.show("slow");
        var display = off.css("display");
        if (display != "none") {
            off.attr("style", "display:none");
        }
    }
    $('#btn-friends').on('click', function () {
        toggle($(".accounts"), $(".groups"))
    });
    $('#btn-groups').on('click', function () {
        toggle($(".groups"), $(".accounts"))
    });
    //-----------------------------------------------

    var getRequest = function ( section, type, a) {
        var $sectionItem = section.find(".search-result-item:last").clone();
        section.empty();
        var url = window.ctx + "/search/"+ type+"?part="+ window.part +"&num=" + a.text()
        $.get( url, function (data) {
                data.forEach(function (e) {
                    var sectionItemClone = $sectionItem.clone();
                    sectionItemClone.find('.view-profile').attr('href', window.ctx + "/"+ type +"/" + e.id);
                    sectionItemClone.find('.search-result-item-heading').text(e.desc);
                    if(e.photoString != null){
                        sectionItemClone.find('.image').attr('src', "data:image/jpg;base64," + e.photoString);
                    } else {
                        sectionItemClone.find('.image').attr('src', window.imgUrl);
                    }
                    section.append(sectionItemClone);
                });
            }
        );
    }

    $('.account-page').on('click', function () {
        getRequest($(".accounts-section"), "account", $(this));
    });
    $('.group-page').on('click', function () {
        getRequest($(".groups-section"), "group", $(this));
    });
});