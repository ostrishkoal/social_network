
$(document).ready(function () {
    //-----------------------------------------------
    var toggle = function (on, off1, off2) {
        on.show("slow");
        off1.attr("style", "display:none");
        off2.attr("style", "display:none");
    }

    $('#btn-groups').on('click', function () {
        toggle($(".groups"), $(".reqs-from"), $(".reqs-to"))
    });

    $('#btn-reqs-from').on('click', function () {
        toggle( $(".reqs-from"), $(".reqs-to"), $(".groups"))
    });

    $('#btn-reqs-to').on('click', function () {
        toggle( $(".reqs-to"), $(".reqs-from"), $(".groups"))
    });
    //-----------------------------------------------


    var getRequest = function ( section, type, a) {
        var $sectionItem = section.find(".search-result-item:last").clone();
        section.empty();
        var url = window.ctx + "/gp/"+ type+"?num=" + a.text()
        $.get( url, function (data) {
                data.forEach(function (e) {
                    var sectionItemClone = $sectionItem.clone();
                    sectionItemClone.find('.image').attr('src', "data:image/jpg;base64," + e.photoString);
                    sectionItemClone.find('.view-profile').attr('href', window.ctx + "/group/" + e.id);
                    sectionItemClone.find('.search-result-item-heading').text(e.desc);
                    section.append(sectionItemClone);
                });
            }
        );
    }

    $('.req-from-page').on('click', function () {
        getRequest($(".reqs-from-section"), "req-from", $(this));
    });
    $('.req-to-page').on('click', function () {
        getRequest($(".reqs-to-section"), "req-to", $(this));
    });
    $('.group-page').on('click', function () {
        getRequest($(".groups-section"), "group", $(this));
    });
});