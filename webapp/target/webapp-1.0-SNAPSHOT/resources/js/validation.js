$(document).ready(function () {
    $form = $("myform");

    $(document).on("click", "#ok", function () {
        var errMssg = '';

        if (!validateEmail()) {
            errMssg += '<p>Invalid email</p>';
        }
        if (!validatePassword()) {
            errMssg += '<p>Invalid password</p>';
        }
        if (!validatePhoneAll()) {
            errMssg += '<p>Invalid phones</p>';
        }

        if (errMssg.length > 0) {
            bootbox.dialog({
                title: "Errors",
                buttons: {
                    close: {
                        label: 'Close',
                        className: "btn btn-sm btn-danger",
                        callback: function () {
                        }
                    }
                },
                message: '<div class="row"><div class="col-md-12"> ' + errMssg + '</div>  </div>'
            });
        } else {
            bootbox.dialog({
                title: "Confirm you changes",
                buttons: {
                    close: {
                        label: 'Close',
                        className: "btn btn-sm btn-danger",
                        callback: function () {
                        }
                    },
                    success: {
                        label: "Submit",
                        className: "btn btn-sm btn-primary",
                        callback: function () {
                            $("#myform").submit();
                        }
                    }
                },
                message: '<div class="row">  ' +
                '<div class="col-md-12"> ' +
                '<p>Are you sure?</p>' +
                '</div>  </div>'
            });
        }
    });

    function validateEmail() {
        var email = document.getElementById('email')
        return email.value.match('@');
    };


    function validatePassword() {
        var password = document.getElementById('password')
        return password.value.length > 0;
    };

    function validatePhone(phone_number) {
        return phone_number.match(/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/);
    };

    function validatePhoneAll() {
        var btns = document.getElementsByClassName("btn-remove");
        for (var i = 0; i < btns.length; i++) {
            var nodes = btns[i].parentNode.parentNode.childNodes;
            for (var j = 0; j < nodes.length; j++) {
                if (nodes[j].className != undefined) {
                    if (nodes[j].className.indexOf("number") > -1 && !validatePhone(nodes[j].value)) {
                        return false;
                    }
                }
            }
        }
        return true;
    };
});