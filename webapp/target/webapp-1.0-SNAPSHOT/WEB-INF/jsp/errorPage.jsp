<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <spring:url value="/resources/font-awesome/css/font-awesome.css" var="fontAwesomeCss"/>
    <link href="${fontAwesomeCss}" rel="stylesheet"/>
    <spring:url value="/resources/css/errorPage.css" var="errorPageCss"/>
    <link href="${errorPageCss}" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-template">
                <h1>Oops!</h1>
                <h2>Something went wrong</h2>
                <div class="error-details"/>
                <div class="error-actions">
                    <a href="${pageContext.request.contextPath}/home" class="btn btn-primary btn-lg">
                        <span class="glyphicon glyphicon-home"></span>
                        Take Me Home
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html> 