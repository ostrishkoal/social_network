
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <c:import url="/WEB-INF/jsp/header.jsp"/>

    <spring:url value="/resources/js/groups.js" var="groupsJs"/>
    <script src="${groupsJs}"></script>

    <spring:url value="/resources/css/list.css" var="listCss"/>
    <link href="${listCss}" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <div class="row ng-scope">
        <div class="col-md-3 col-md-push-9">
            <h4>Results <span class="fw-semi-bold">Filtering</span></h4>
            <p class="text-muted fs-mini">Listed content is categorized by the following groups:</p>
            <ul class="nav nav-pills nav-stacked search-result-categories mt">
                <li>
                    <a id="btn-groups">Groups<span class="badge">${cntGroups}</span></a>
                </li>
                <li>
                    <a id="btn-reqs-from">Requests<span class="badge">${cntReqsFrom}</span></a>
                </li>
            </ul>
        </div>
        <div class="col-md-9 col-md-pull-3 result-list">
            <p class="search-results-count groups">About ${cntGroups} groups</p>
            <p class="search-results-count reqs-from" style="display: none;">About ${cntReqsFrom} request from me</p>

            <div class="groups groups-section">
                <c:forEach items="${groups}" var="element">
                    <section class="search-result-item" >
                        <a class="image-link" href="#">
                            <c:set var="url" value="/resources/img/avatar1.jpg"/>
                            <c:if test="${element.photoString != null}">
                                <c:set var="url" value="data:image/jpg;base64,${element.photoString}"/>
                            </c:if>
                            <img class="image" src="${url}"/>
                        </a>
                        <div class="search-result-item-body">
                            <div class="row">
                                <div class="col-sm-9">
                                    <h4 class="search-result-item-heading">
                                        <a href="#">${element.name}</a>
                                    </h4>
                                    <p class="info">profile group</p>
                                </div>
                                <div class="col-sm-3 text-align-center">
                                    <a class="btn btn-primary btn-info btn-sm view-profile" href="${pageContext.request.contextPath}/group/${element.id}">View profile</a>
                                </div>
                            </div>
                        </div>
                    </section>
                </c:forEach>
            </div>

            <div class="reqs-from reqs-from-section" style="display: none;">
                <c:forEach items="${reqsFrom}" var="element">
                    <section class="search-result-item" >
                        <a class="image-link" href="#">
                            <c:set var="url" value="/resources/img/avatar1.jpg"/>
                            <c:if test="${element.photoString != null}">
                                <c:set var="url" value="data:image/jpg;base64,${element.photoString}"/>
                            </c:if>
                            <img class="image" src="${url}"/>
                        </a>
                        <div class="search-result-item-body">
                            <div class="row">
                                <div class="col-sm-9">
                                    <h4 class="search-result-item-heading">
                                        <a href="#">${element.name}</a>
                                    </h4>
                                    <p class="info">profile group</p>
                                </div>
                                <div class="col-sm-3 text-align-center">
                                    <a class="btn btn-primary btn-info btn-sm view-profile" href="${pageContext.request.contextPath}/group/${element.id}">View profile</a>
                                </div>
                            </div>
                        </div>
                    </section>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
</body>
</html>