<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <c:import url="/WEB-INF/jsp/header.jsp"/>

    <spring:url value="/resources/font-awesome/css/font-awesome.css" var="fontAwesomeCss"/>
    <link href="${fontAwesomeCss}" rel="stylesheet"/>
    <spring:url value="/resources/css/page.css" var="PageCss"/>
    <link href="${PageCss}" rel="stylesheet"/>

    <spring:url value="/resources/js/friend.js" var="friend"/>
    <script src="${friend}"></script>

    <spring:url value="/resources/img/avatar1.jpg" var="imgUrl"/>
</head>
<body>
<div id="user-profile-2" class="user-profile">
    <div class="tab-content no-border padding-24">
        <div id="home" class="tab-pane in active">
            <div class="row">
                <div class="col-xs-12 col-sm-3 center">
							<span class="profile-picture">
                                    <c:if test="${thisAccount.photoString != null}">
                                        <c:set var="imgUrl" value="data:image/jpg;base64,${thisAccount.photoString}"/>
                                    </c:if>
								<img class="img-responsive" alt=" Avatar" id="avatar2" src="${imgUrl}">
							</span>

                    <div class="space space-4"></div>

                    <c:if test="${thisAccount.id != sessionScope.account.id}">
                        <c:set var="relation" value="${sessionScope.account.getRelation(thisAccount)}"/>

                        <%--${relation}--%>

                        <c:if test="${relation.getStatus() == 'DEFAULT'}">
                            <button type="button" class="btn btn-sm btn-block btn-req">Add as a friend</button>
                        </c:if>
                        <c:if test="${relation.getStatus() == 'ACCEPTED'}">
                            <button type="button" class="btn btn-sm btn-block btn-req">Remove friend</button>
                        </c:if>
                        <c:if test="${(relation.getStatus() == 'PENDING') && (relation.isOwner())}">
                            <button type="button" class="btn btn-sm btn-block btn-req">Decline friend request</button>
                        </c:if>
                        <c:if test="${(relation.getStatus() == 'PENDING') && (!relation.isOwner())}">
                            <button type="button" class="btn btn-sm btn-block btn-decline btn-req">Decline friend request</button>
                            <button type="button" class="btn btn-sm btn-block btn-accept btn-req">Accept friend request</button>
                        </c:if>

                        <a href="${pageContext.request.contextPath}/account/chat/${thisAccount.id}" class="btn btn-sm btn-block btn-primary">
                            <i class="ace-icon fa fa-envelope-o bigger-110"></i>
                            <span class="bigger-110">Send a message</span>
                        </a>
                    </c:if>
                </div><!-- /.col -->

                <div class="col-xs-12 col-sm-9">
                    <h4 class="blue">
                        <span class="middle">${thisAccount.name} ${thisAccount.surname}</span>

                        <span class="label label-purple arrowed-in-right">
									<i class="ace-icon fa fa-circle smaller-80 align-middle"></i>
									online
								</span>
                    </h4>

                    <div class="profile-user-info">
                        <div class="profile-info-row">
                            <div class="profile-info-name">
                                <i class="fa fa-user light-black bigger-110"></i>
                            </div>

                            <div class="profile-info-value">
                                <span>#profile</span>
                            </div>
                        </div>

                        <div class="profile-info-row">
                            <div class="profile-info-name">
                                <i class="fa fa-birthday-cake light-black bigger-110"></i>
                            </div>

                            <div class="profile-info-value">
										<span>
											<fmt:formatDate value="${thisAccount.dateBirth}" pattern="dd-mm-yyyy"/>
										</span>
                            </div>
                        </div>

                        <div class="profile-info-row">
                            <div class="profile-info-name">
                                <i class="fa fa-number light-black bigger-110"></i>
                            </div>
                            <div class="profile-info-value">
                                <table>
                                    <c:forEach items="${thisAccount.phones}" var="element">
                                        <tr>
                                            <td>${element.type}</td>
                                            <td>${element.number}</td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="hr hr-8 dotted"></div>

                    <div class="profile-user-info">

                        <div class="profile-info-row">
                            <div class="profile-info-name">
                                <i class="middle ace-icon fa fa-vk bigger-150 black"></i>
                            </div>

                            <div class="profile-info-value">
                                <a href="#">Find me on Vk</a>
                            </div>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->

            <div class="space-20"></div>

            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-small">
                            <h4 class="widget-title smaller">
                                <i class="ace-icon fa fa-check-square-o bigger-110"></i>
                                Little About Me
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <p>
                                    Personal information.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /#home -->
    </div>
</div>
<script>
    window.id = "${thisAccount.id}";
</script>
</body>
</html>



