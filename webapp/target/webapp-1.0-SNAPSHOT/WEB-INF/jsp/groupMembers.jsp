<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <c:import url="/WEB-INF/jsp/header.jsp"/>

    <spring:url value="/resources/css/list.css" var="listCss"/>
    <link href="${listCss}" rel="stylesheet"/>

    <spring:url value="/resources/js/groupMembers.js" var="groupMembers"/>
    <script src="${groupMembers}"></script>

    <spring:url value="/resources/img/avatar1.jpg" var="imgUrl"/>
</head>
<body>
<div class="container">
    <div class="row ng-scope">
        <div class="col-md-3 col-md-push-9">
            <h4>Results <span class="fw-semi-bold">Filtering</span></h4>
            <p class="text-muted fs-mini">Listed content is categorized by the following groups:</p>
            <ul class="nav nav-pills nav-stacked search-result-categories mt">
                <li>
                    <a id="btn-members">Members <span class="badge">${members.size()}</span></a>
                </li>
                <c:if test="${group.getRelationRole(account) == 'ADMIN' || group.creator.equals(account)}">
                    <li>
                        <a id="btn-followers">Followers<span class="badge">${followers.size()}</span></a>
                    </li>
                </c:if>
            </ul>
        </div>
        <div class="col-md-9 col-md-pull-3 result-list">
            <p class="search-results-count members">About ${members.size()} members</p>
            <p class="search-results-count followers" style="display: none;">About ${followers.size()} followers</p>

            <div id="members" class="members members-section">
                <c:forEach items="${members}" var="element">
                    <section class="search-result-item">
                        <a class="image-link" href="#">
                            <c:set var="url" value="imgUrl"/>
                            <c:if test="${element.photoString != null}">
                                <c:set var="url" value="data:image/jpg;base64,${element.photoString}"/>
                            </c:if>
                            <img class="image" src="${url}"/>
                        </a>
                        <div class="search-result-item-body">
                            <div class="row">
                                <div class="col-sm-9">
                                    <h4 class="search-result-item-heading">
                                        <a href="#">${element.name} ${element.surname}</a>
                                    </h4>
                                    <p class="info">profile account</p>
                                </div>
                                <div class="col-sm-3 text-align-center">
                                    <a class="btn btn-primary btn-block btn-sm view-profile"
                                       href="${pageContext.request.contextPath}/account/${element.id}">View profile</a>
                                    <button type="button" class="btn btn-sm btn-block btn-req">Remove</button>
                                </div>
                            </div>
                        </div>
                    </section>
                </c:forEach>
            </div>

            <c:if test="${group.getRelationRole(account) == 'ADMIN' || group.creator.equals(account)}">
                <div class="followers followers-section" style="display: none;">
                    <c:forEach items="${followers}" var="element">
                        <section class="search-result-item">
                            <a class="image-link" href="#">
                                <c:set var="url" value="imgUrl"/>
                                <c:if test="${element.photoString != null}">
                                    <c:set var="url" value="data:image/jpg;base64,${element.photoString}"/>
                                </c:if>
                                <img class="image" src="${url}"/>
                            </a>
                            <div class="search-result-item-body">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <h4 class="search-result-item-heading">
                                            <a href="#">${element.name}</a>
                                        </h4>
                                        <p class="info">profile account</p>
                                    </div>
                                    <div class="col-sm-3 text-align-center">
                                        <a class="btn btn-primary btn-block btn-sm view-profile"
                                           href="${pageContext.request.contextPath}/account/${element.id}">View profile
                                        </a>
                                        <button type="button" class="btn btn-decline btn-sm btn-block btn-req">Decline</button>
                                        <button type="button" class="btn btn-accept btn-sm btn-block btn-req">Accept</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </c:forEach>
                </div>
            </c:if>
        </div>
    </div>
</div>
<script>
    window.groupId = "${group.id}";
</script>
</body>
</html>
