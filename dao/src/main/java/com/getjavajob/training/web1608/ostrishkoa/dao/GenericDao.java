package com.getjavajob.training.web1608.ostrishkoa.dao;

import com.getjavajob.training.web1608.ostrishkoa.core.additions.Identity;
import com.getjavajob.training.web1608.ostrishkoa.exception.DAOException;

import java.util.List;

public interface GenericDao<T extends Identity<K>, K> {
    T find(K key) throws DAOException;

    List<T> list() throws DAOException;

    void create(T obj) throws IllegalArgumentException, DAOException;

    void update(T obj) throws IllegalArgumentException, DAOException;

    void delete(T obj) throws DAOException;
}
