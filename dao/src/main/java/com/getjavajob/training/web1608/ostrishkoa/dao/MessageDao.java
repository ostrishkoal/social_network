package com.getjavajob.training.web1608.ostrishkoa.dao;

import com.getjavajob.training.web1608.ostrishkoa.core.*;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.*;

@Repository
public class MessageDao {
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    protected EntityManager em;

    public void delete(Message msg) {
        em.remove(find(msg.getId()));
    }

    public void create(Message msg) {
        em.persist(msg);
    }

    public void update(Message msg) {
        em.merge(msg);
    }

    public Message find(Integer id) {
        return em.find(Message.class, id);
    }

    public List<Message> list(Account sender, Account receiver, int cnt) { //todo test
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Message> query = builder.createQuery(Message.class);
        Root<Message> from = query.from(Message.class);
        Predicate predicate = builder.or(builder.and(builder.equal(from.get("accountFrom"), sender.getId()), builder.equal(from.get("accountTo"), receiver.getId())), builder.and(builder.equal(from.get("accountTo"), sender.getId()), builder.equal(from.get("accountFrom"), receiver.getId())));
        query.select(from).where(builder.and(new Predicate[]{predicate})).orderBy(builder.desc(from.get("id")));
        TypedQuery<Message> typedQuery = em.createQuery(query);
        typedQuery.setMaxResults(cnt);
        List<Message> list = typedQuery.getResultList();
        Collections.reverse(list);
        return list;
    }
}
