package com.getjavajob.training.web1608.ostrishkoa.dao;

import com.getjavajob.training.web1608.ostrishkoa.core.Account;
import com.getjavajob.training.web1608.ostrishkoa.core.additions.Status;
import com.getjavajob.training.web1608.ostrishkoa.core.relations.AccountRelation;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;

@Repository
public class AccountDao extends AbstractDao<Account> {
    @Override
    public Class getClazz() {
        return Account.class;
    }

    public Account findByEmail(String email) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Account> query = builder.createQuery(Account.class);
        Root<Account> from = query.from(Account.class);
        Predicate predicate = builder.equal(from.get("email"), email);
        query.select(from).where(builder.and(new Predicate[]{predicate}));
        TypedQuery<Account> typedQuery = em.createQuery(query);
        return typedQuery.getSingleResult();
    }

    public Account findByUUID(String uuid) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Account> query = builder.createQuery(Account.class);
        Root<Account> from = query.from(Account.class);
        Predicate predicate = builder.equal(from.get("uuid"), uuid);
        query.select(from).where(builder.and(new Predicate[]{predicate}));
        TypedQuery<Account> typedQuery = em.createQuery(query);
        return typedQuery.getSingleResult();
    }

    public List<Account> findByPartName(String part) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Account> query = builder.createQuery(Account.class);
        Root<Account> from = query.from(Account.class);
        Predicate predicate = builder.or(builder.like(builder.lower(from.get("name")), "%" + part + "%"), builder.like(builder.lower(from.get("surname")), "%" + part + "%"));
        query.select(from).where(builder.and(new Predicate[]{predicate}));
        TypedQuery<Account> typedQuery = em.createQuery(query);
        return typedQuery.getResultList();
    }

    public Long getCntByPartName(String part) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Account> from = query.from(Account.class);
        Predicate predicate = builder.or(builder.like(builder.lower(from.get("name")), "%" + part + "%"), builder.like(builder.lower(from.get("surname")), "%" + part + "%"));
        query.select(builder.count(from)).where(predicate);
        return em.createQuery(query).getSingleResult();
    }

    public List<Account> findByPartName(String part, int cntLimit, int pageNamber) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Account> query = builder.createQuery(Account.class);
        Root<Account> from = query.from(Account.class);
        Predicate predicate = builder.or(builder.like(builder.lower(from.get("name")), "%" + part + "%"), builder.like(builder.lower(from.get("surname")), "%" + part + "%"));
        query.select(from).where(builder.and(new Predicate[]{predicate}));
        TypedQuery<Account> typedQuery = em.createQuery(query);
        typedQuery.setFirstResult(cntLimit * (pageNamber - 1));
        typedQuery.setMaxResults(cntLimit);
        return typedQuery.getResultList();
    }

    public void sendFriendReq(Account accountFrom, Account accountTo) {
        Optional<AccountRelation> accountRelation = accountFrom.getAccountRelationFrom(accountTo);
        if (!accountRelation.isPresent()) {
            accountRelation = accountFrom.getAccountRelationTo(accountTo);
            if (accountRelation.isPresent()) {
                accountRelation.get().swap();
            }
        }
        if (accountRelation.isPresent()) {
            accountRelation.get().setStatus(Status.PENDING);
            em.merge(accountRelation.get());
            return;
        }

        AccountRelation newAccountRelation = new AccountRelation(Status.PENDING, accountFrom, accountTo);
        accountFrom.getRelationsFrom().add(newAccountRelation);
        accountTo.getRelationsTo().add(newAccountRelation);
        em.persist(newAccountRelation);
    }

    public void acceptFriend(Account accountFrom, Account accountTo) {
        Optional<AccountRelation> accountRelationTo = accountFrom.getAccountRelationTo(accountTo);
        if (accountRelationTo.isPresent()) {
            accountRelationTo.get().setStatus(Status.ACCEPTED);
            em.merge(accountRelationTo.get());
        }
    }

    public void declineFriend(Account accountFrom, Account accountTo) {
        Optional<AccountRelation> accountRelation = accountFrom.getAccountRelationFrom(accountTo);
        if (!accountRelation.isPresent()) {
            accountRelation = accountFrom.getAccountRelationTo(accountTo);
            if (accountRelation.isPresent()) {
                accountRelation.get().swap();

                accountFrom.getRelationsFrom().add(accountRelation.get());
                accountFrom.getRelationsTo().remove(accountRelation.get());
                accountTo.getRelationsFrom().remove(accountRelation.get());
                accountTo.getRelationsTo().add(accountRelation.get());
            }
        }
        if (accountRelation.isPresent()) {
            accountRelation.get().setStatus(Status.DEFAULT);
            em.merge(accountRelation.get());
        }
    }
}