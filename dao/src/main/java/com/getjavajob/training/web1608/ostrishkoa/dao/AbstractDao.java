package com.getjavajob.training.web1608.ostrishkoa.dao;

import com.getjavajob.training.web1608.ostrishkoa.core.additions.Identity;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.List;

@Repository
public abstract class AbstractDao<T extends Identity<Integer>> implements GenericDao<T, Integer> {
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    //@PersistenceContext
    protected EntityManager em;

    public abstract Class<T> getClazz();

    @Override
    public void delete(T obj){
        em.remove(find(obj.getId()));
    }

    @Override
    public void create(T obj){
        em.persist(obj);
    }

    @Override
    public void update(T obj) {
        em.merge(obj);
    }

    public T find(Integer id) {
        return em.find(getClazz(), id);
    }

    public List<T> list() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(getClazz());
        Root<T> rootEntry = cq.from(getClazz());
        CriteriaQuery<T> all = cq.select(rootEntry);
        TypedQuery<T> allQuery = em.createQuery(all);
        return allQuery.getResultList();
    }
}
