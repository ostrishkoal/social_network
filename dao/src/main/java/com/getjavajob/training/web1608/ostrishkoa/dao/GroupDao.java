package com.getjavajob.training.web1608.ostrishkoa.dao;

import com.getjavajob.training.web1608.ostrishkoa.core.*;
import com.getjavajob.training.web1608.ostrishkoa.core.additions.*;
import com.getjavajob.training.web1608.ostrishkoa.core.relations.*;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;

@Repository
public class GroupDao extends AbstractDao<Group> {
    @Override
    public Class<Group> getClazz() {
        return Group.class;
    }

    public List<Group> findByName(String name) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> from = query.from(Group.class);
        Predicate predicate = builder.equal(from.get("name"), name);
        query.select(from).where(builder.and(new Predicate[]{predicate}));
        TypedQuery<Group> typedQuery = em.createQuery(query);
        return typedQuery.getResultList();
    }

    public List<Group> findByAccount(Account account) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> from = query.from(Group.class);
        Predicate predicate = builder.equal(from.get("creator"), account.getId());
        query.select(from).where(builder.and(new Predicate[]{predicate}));
        TypedQuery<Group> typedQuery = em.createQuery(query);
        return typedQuery.getResultList();
    }

    public List<Group> findByPartName(String part) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> from = query.from(Group.class);
        Predicate predicate = builder.like(builder.lower(from.get("name")), "%" + part + "%");
        query.select(from).where(builder.and(new Predicate[]{predicate}));
        TypedQuery<Group> typedQuery = em.createQuery(query);
        return typedQuery.getResultList();
    }

    public List<Group> findByPartName(String part, int cntLimit, int pageNamber) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Group> query = builder.createQuery(Group.class);
        Root<Group> from = query.from(Group.class);
        Predicate predicate = builder.like(builder.lower(from.get("name")), "%" + part + "%");
        query.select(from).where(builder.and(new Predicate[]{predicate}));
        TypedQuery<Group> typedQuery = em.createQuery(query);
        typedQuery.setFirstResult(cntLimit * (pageNamber - 1));
        typedQuery.setMaxResults(cntLimit);
        return typedQuery.getResultList();
    }

    public Long getCntByPartName(String part) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Group> from = query.from(Group.class);
        Predicate predicate = builder.like(builder.lower(from.get("name")), "%" + part + "%");
        query.select(builder.count(from)).where(predicate);
        return em.createQuery(query).getSingleResult();
    }

    public void sendGroupReq(Account account, Group group) {
        Optional<GroupRelation> groupRelation = group.getGroupRelation(account);
        if (groupRelation.isPresent()) {
            groupRelation.get().setStatus(Status.PENDING);
            em.merge(groupRelation.get());
            return;
        }
        GroupRelation newRelation = new GroupRelation(Status.PENDING, account, group);
        account.getGroupsRelation().add(newRelation); //todo где хранить связь
        group.getGroupRelations().add(newRelation);
        em.persist(newRelation);
    }

    public void acceptMember(Account account, Group group) {
        Optional<GroupRelation> groupRelation = group.getGroupRelation(account);
        if (groupRelation.isPresent()) {
            groupRelation.get().setStatus(Status.ACCEPTED);
            em.merge(groupRelation.get());
        }
    }

    public void declineMember(Account account, Group group) {
        Optional<GroupRelation> groupRelation = group.getGroupRelation(account);
        if (groupRelation.isPresent()) {
            groupRelation.get().setStatus(Status.DEFAULT);
            groupRelation.get().setRole(Role.USER);
            em.merge(groupRelation.get());
        }
    }

    public void setAdmin(Account account, Group group) {
        Optional<GroupRelation> groupRelation = group.getGroupRelation(account);
        if (groupRelation.isPresent() && groupRelation.get().getStatus() == Status.ACCEPTED) {
            groupRelation.get().setRole(Role.ADMIN);
            em.merge(groupRelation.get());
        }
    }
}
