package com.getjavajob.training.web1608.ostrishkoa;

import org.apache.commons.dbcp.BasicDataSource;

import java.net.*;
import java.sql.*;

public class TmpTest {
    public static void main(String[] args) throws SQLException, URISyntaxException {
        URI dbUri = new URI("mysql://b719153c195a47:5f0379c9@us-cdbr-iron-east-04.cleardb.net/heroku_687631b0c22d188?reconnect=true");
//        Class.forName("");

        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        String dbUrl = "jdbc:mysql://" + dbUri.getHost() + dbUri.getPath();

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setUrl(dbUrl);
        basicDataSource.setUsername(username);
        basicDataSource.setPassword(password);

//        System.out.println(basicDataSource);
    }
}
