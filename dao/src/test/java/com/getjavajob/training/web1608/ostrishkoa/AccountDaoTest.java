package com.getjavajob.training.web1608.ostrishkoa;

import com.getjavajob.training.web1608.ostrishkoa.core.*;
import com.getjavajob.training.web1608.ostrishkoa.core.additions.*;
import com.getjavajob.training.web1608.ostrishkoa.dao.AccountDao;
import com.getjavajob.training.web1608.ostrishkoa.exception.DAOException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
@Sql(value = {"classpath:create.sql", "classpath:insert.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"classpath:drop.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class AccountDaoTest {
    @Autowired
    protected AccountDao accountDao;

    @Test
    public void findAccountByEmail() throws DAOException {
        Account account = accountDao.findByEmail("kate@mail.ru");
        assertEquals(account.getPassword(), "123");
    }

    @Test
    public void findAccountById() throws DAOException {
        Account account = accountDao.find(1);
        assertEquals(account.getEmail(), "kate@mail.ru");
    }

    @Test
    public void findAccountNull() throws DAOException {
        assertNull(accountDao.find(5));
    }

    @Test
    public void listAccount() throws DAOException {
        List<Account> list = accountDao.list();
        assertEquals(list.size(),4);
    }

//    @Test
//    @Transactional
//    public void createAccount() throws DAOException {
//        Account account = new Account();
//        account.setEmail("1@mail.ru");
//        account.setPassword("123");
//        Phone phone = new Phone("11-11");
//        account.addPhone(phone);
//        accountDao.create(account);
//        Account accountFromBase = accountDao.find(account.getId());
//        assertEquals(account, accountFromBase);
//    } todo

//    @Test
//    @Transactional
//    public void updateAccount() throws DAOException {
//        Account account = accountDao.find(1);
//        String email = "upd@mail.ru";
//        account.setEmail(email);
//        Phone phone = new Phone("11-55");
//        account.addPhone(phone);
//        accountDao.update(account);
//        Account accountDB = accountDao.find(1);
//        assertEquals(accountDB, account);
//    } todo

    @Test
    @Transactional
    public void deleteAccount() throws DAOException {
        Account account = accountDao.find(1);
        accountDao.delete(account);
        Account accountFromBase = accountDao.find(1);
        assertNull(accountFromBase);
    }


    @Test
    public void findPhones() {
        Phone phone = new Phone("55-55");
        phone.setType("WORK");
        List<Phone> list = new ArrayList<>(asList(phone));
        Account account = accountDao.find(1);
        List<Phone> listPhones = account.getPhones();
        assertEquals(list, listPhones);
    }

    //        @Test
    //        public void findAddress() {
    //            List<Address> list = new ArrayList<>(asList(new Address("moscow")));
    //            Account account = accountDao.findByEmail(1);
    //            List<Address> listDB = accountDao.findAddresses(account);
    //            assertEquals(list, listDB);
    //        }

    @Test
    @Transactional
    public void findRelation() {
        Account accountOne = accountDao.find(1);
        Account accountTwo = accountDao.find(2);
        Relation relation = accountOne.getRelation(accountTwo);
        assertEquals(relation, new Relation(Status.PENDING, true));
    }

//    @Test
//    @Transactional
//    public void sendFriendReq() {
//        Account accountOne = accountDao.find(1);
//        Account accountTwo = accountDao.find(3);
//        accountDao.sendFriendReq(accountOne, accountTwo);
////        Relation relation = accountOne.getRelation(accountTwo);
////        assertEquals(relation, new Relation(Status.PENDING, true));
//    } todo before commit ID is null

    @Test
    @Transactional
    public void acceptFriend() {
        Account accountOne = accountDao.find(2);
        Account accountTwo = accountDao.find(1);
        accountDao.acceptFriend(accountOne, accountTwo);
        Relation relation = accountOne.getRelation(accountTwo);
        assertEquals(relation, new Relation(Status.ACCEPTED, false));
    }

    @Test
    @Transactional
    public void declineFriend() {
        Account accountOne = accountDao.find(2);
        Account accountTwo = accountDao.find(1);
        accountDao.declineFriend(accountOne, accountTwo);
        Relation relation = accountOne.getRelation(accountTwo);
        assertEquals(relation, new Relation(Status.DEFAULT, true));
    }

    @Test
    @Transactional
    public void listFriends() throws DAOException {
        Account accountOne = accountDao.find(2);
        Account accountTwo = accountDao.find(3);
        Set<Account> listFriends = accountOne.getFriends();
        assertEquals(listFriends, new HashSet<>(asList(accountTwo)));
    }

    @Test
    @Transactional
    public void listFollowers() throws DAOException {
        Account accountOne = accountDao.find(2);
        Account accountTwo = accountDao.find(1);
        Set<Account> listFollowers = accountOne.getFollowers();
        assertEquals(listFollowers, new HashSet<>(asList(accountTwo)));
    }

    @Test
    @Transactional
    public void listFollowed() throws DAOException {
        Account accountOne = accountDao.find(4);
        Account accountTwo = accountDao.find(1);
        Set<Account> listFollowed = accountOne.getFollowed();
        assertEquals(listFollowed, new HashSet<>(asList(accountTwo)));
    }
}
