package com.getjavajob.training.web1608.ostrishkoa;

import com.getjavajob.training.web1608.ostrishkoa.core.*;
import com.getjavajob.training.web1608.ostrishkoa.core.additions.*;
import com.getjavajob.training.web1608.ostrishkoa.dao.*;
import com.getjavajob.training.web1608.ostrishkoa.exception.DAOException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
@Sql(value = {"classpath:create.sql", "classpath:insert.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"classpath:drop.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class GroupDaoTest {
    @Autowired
    protected AccountDao accountDao;
    @Autowired
    private GroupDao groupDao;

    @Test
    public void findGroupByName() throws DAOException {
        List<Group> groups = groupDao.findByName("otp");
        Account account = accountDao.find(1);
        Group group = new Group("otp", account);
        assertEquals(groups, new ArrayList<>(asList(group)));
    }

    @Test
    public void findGroupNull() throws DAOException {
        assertNull(groupDao.find(121));
    }

    @Test
    public void listGroup() throws DAOException {
        List<Group> list = groupDao.list();
        assertEquals(list.size(), 4);
    }

//    @Test
//    @Transactional
//    public void createGroup() throws DAOException {
//        Group group = new Group();
//        group.setId(121);
//        group.setName("gjj");
//        Account account = accountDao.find(1);
//        group.setCreator(account);
//        groupDao.create(group);
//        Group groupFromBase = groupDao.find(group.getId());
//        assertEquals(group, groupFromBase);
//    } todo

    @Test
    @Transactional
    public void updateGroup() throws DAOException {
        Group group = groupDao.find(1);
        String name = "moscow";
        group.setName(name);
        groupDao.update(group);

        Group groupDB = groupDao.find(1);
        assertEquals(groupDB, group);
    }

    @Test
    @Transactional
    public void deleteGroup() throws DAOException {
        Group group = groupDao.find(1);
        groupDao.delete(group);
        assertNull(groupDao.find(1));
    }

    @Test
    @Transactional
    public void findMembers() throws DAOException {
        Account account = accountDao.find(3);
        Group group = groupDao.find(2);
        assertEquals(new HashSet<>(asList(account)), group.getMembers());
    }

    @Test
    @Transactional
    public void findRelation() {
        Account account = accountDao.find(3);
        Group group = groupDao.find(2);
        assertEquals(group.getRelationStatus(account), Status.ACCEPTED);
    }

    //        @Test
    //        @Transactional
    //        public void sendReq() {
    //            Group group = groupDao.find(2);
    //            Account account = accountDao.find(3);
    //            groupDao.sendGroupReq(account, group);
    //            Status status = group.getRelationStatus(account);
    //            assertEquals(status, Status.PENDING);
    //        } //todo before commit ID is null

    @Test
    @Transactional
    public void acceptFriend() {
        Account account = accountDao.find(2);
        Group group = groupDao.find(1);
        groupDao.acceptMember(account, group);
        assertEquals(group.getRelationStatus(account), Status.ACCEPTED);
    }

    @Test
    @Transactional
    public void declineFriend() {
        Account account = accountDao.find(3);
        Group group = groupDao.find(2);
        groupDao.declineMember(account, group);
        assertEquals(group.getRelationStatus(account), Status.DEFAULT);
    }

    @Test
    @Transactional
    public void setAdmin() {
        Account account = accountDao.find(3);
        Group group = groupDao.find(2);
        groupDao.setAdmin(account, group);
        assertEquals(group.getRelationRole(account), Role.ADMIN);
    }

    @Test
    public void listMembers() throws DAOException {
        Account account = accountDao.find(3);
        Group group = groupDao.find(2);
        Set<Account> listMembers = group.getMembers();
        assertEquals(listMembers, new HashSet<>(asList(account)));
    }

    @Test
    public void listFollowers() throws DAOException {
        Account account = accountDao.find(2);
        Group group = groupDao.find(1);
        Set<Account> listFollowers = group.getFollowers();
        assertEquals(listFollowers, new HashSet<>(asList(account)));
    }

    @Test
    public void listCreatedGroups() throws DAOException {
        Account account = accountDao.find(1);
        Group groupOne = groupDao.find(1);
        Group groupTwo = groupDao.find(2);
        Set<Group> listCreatedGroups = account.getCreatedGroups();
        assertEquals(listCreatedGroups, new HashSet<>(asList(groupOne, groupTwo)));
    }

}
