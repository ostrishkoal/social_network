CREATE SCHEMA IF NOT EXISTS network;

CREATE TABLE IF NOT EXISTS network.ref_account(
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) DEFAULT NULL,
  surname VARCHAR(100) DEFAULT NULL,
  date_birth DATE DEFAULT NULL,
  sex char(1) DEFAULT NULL,
  email VARCHAR(100) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  uuid VARCHAR(100) NULL,
  icq VARCHAR(100) DEFAULT NULL,
  skype VARCHAR(100) DEFAULT NULL,
  hobby VARCHAR(100) DEFAULT NULL,
  education VARCHAR(100) DEFAULT NULL,
  employment VARCHAR(100) DEFAULT NULL,
  status int DEFAULT NULL,
  photo MEDIUMBLOB DEFAULT NULL,
  role VARCHAR(20) DEFAULT 'USER',
  PRIMARY KEY (id));

CREATE TABLE IF NOT EXISTS network.ref_group (
id INT  NOT NULL AUTO_INCREMENT ,
name VARCHAR(100) NOT NULL,
description VARCHAR(200) NULL,
CREATOR INT NOT NULL,
photo MEDIUMBLOB DEFAULT NULL,
PRIMARY KEY (id),
FOREIGN KEY (CREATOR) REFERENCES ref_account(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS network.ref_phone(
  id INT NOT NULL AUTO_INCREMENT ,
  number VARCHAR(100) NOT NULL,
  type  VARCHAR(100) NULL,
  PRIMARY KEY (id));

CREATE TABLE IF NOT EXISTS network.phones(
  account_id INT NOT NULL,
  phone_id INT NOT NULL);

-- CREATE TABLE IF NOT EXISTS network.addresses(
--   id INT NOT NULL AUTO_INCREMENT ,
--   address VARCHAR(100) NOT NULL,
--   type  VARCHAR(100) NULL,
--   account_id INT NOT NULL,
--   PRIMARY KEY (id),
--   FOREIGN KEY (account_id) REFERENCES network.ref_account (id) ON DELETE CASCADE);

CREATE TABLE IF NOT EXISTS network.account_relation (
  id INT NOT NULL AUTO_INCREMENT,
  account_from_id INT NOT NULL,
  account_to_id INT NOT NULL,
  status varchar(20) DEFAULT 'DEFAULT',
  PRIMARY KEY (id),
  FOREIGN KEY (account_from_id) REFERENCES network.ref_account(id)ON DELETE CASCADE,
  FOREIGN KEY (account_to_id) REFERENCES network.ref_account(id)ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS network.group_relation (
  id INT NOT NULL AUTO_INCREMENT,
  account_id INT NOT NULL,
  group_id INT NOT NULL,
  status varchar(20) DEFAULT 'DEFAULT',
  role varchar(20) DEFAULT 'USER',
  PRIMARY KEY (id),
  FOREIGN KEY (account_id) REFERENCES network.ref_account(id) ON DELETE CASCADE,
  FOREIGN KEY (group_id) REFERENCES network.ref_group(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS network.messages (
  id INT NOT NULL AUTO_INCREMENT,
  account_from_id INT NOT NULL,
  account_to_id INT NOT NULL,
  message varchar(200) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (account_from_id) REFERENCES network.ref_account(id)ON DELETE CASCADE,
  FOREIGN KEY (account_to_id) REFERENCES network.ref_account(id)ON DELETE CASCADE
);

commit;