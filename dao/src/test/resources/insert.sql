-----------------------------------------
insert into network.ref_account(  id,  name,  surname,  date_birth,  sex,  email,  password  ,icq, skype, hobby, education, employment, status, uuid, photo)
values(  1,  'kate',  null,  date'1993-01-01',  'F',  'kate@mail.ru',  '123'  ,null, null, null, null, null, null, null, null);
insert into network.ref_account(  id,  name,  surname,  date_birth,  sex,  email,  password  ,icq, skype, hobby, education, employment, status, uuid, photo)
values(  2,  'sasha',  null,  date'1992-01-01',  'M',  'sasha@mail.ru',  '321'  ,null, null, null, null, null, null, null, null);
insert into network.ref_account(  id,  name,  surname,  date_birth,  sex,  email,  password  ,icq, skype, hobby, education, employment, status, uuid, photo)
values(  3,  'kot',  null,  date'2000-01-01',  'M',  'kot@mail.ru',  '000'  ,null, null, null, null, null, null, null, null);
insert into network.ref_account(  id,  name,  surname,  date_birth,  sex,  email,  password  ,icq, skype, hobby, education, employment, status, uuid, photo)
values(  4,  'mew',  null,  date'2000-01-01',  'M',  'mew@mail.ru',  '000'  ,null, null, null, null, null, null, null, null);
-----------------------------------------

INSERT INTO network.ref_phone(id,  number,  type) VALUES(1,  '55-55',  'WORK' );
INSERT INTO network.phones(account_id,  phone_id) VALUES(1, 1 );
-- INSERT INTO network.addresses(id,  address,  type,  account_id) VALUES(1,  'moscow',  null,  1  );

INSERT INTO network.ref_group(id, name, description, creator) VALUES(1,'otp', null, 1);
INSERT INTO network.ref_group(id, name, description, creator) VALUES(2,'pokemon', null, 1);
INSERT INTO network.ref_group(id, name, description, creator) VALUES(3,'auto', null, 2);
INSERT INTO network.ref_group(id, name, description, creator) VALUES(4,'vk', null, 2);

INSERT INTO network.account_relation ( account_from_id, account_to_id, status) VALUES (1, 2, 'PENDING');
INSERT INTO network.account_relation ( account_from_id, account_to_id, status) VALUES (2, 3, 'ACCEPTED');
INSERT INTO network.account_relation ( account_from_id, account_to_id, status) VALUES (4, 1, 'PENDING');

INSERT INTO network.group_relation ( account_id, group_id, status) VALUES (2, 1, 'PENDING');
INSERT INTO network.group_relation ( account_id, group_id, status) VALUES (3, 2, 'ACCEPTED');
INSERT INTO network.group_relation ( account_id, group_id, status) VALUES (4, 1, 'DEFAULT');

INSERT INTO network.messages ( account_from_id, account_to_id, message) VALUES (1, 2, 'text1');
INSERT INTO network.messages ( account_from_id, account_to_id, message) VALUES (2, 1, 'text2');
INSERT INTO network.messages ( account_from_id, account_to_id, message) VALUES (1, 2, 'text3');
INSERT INTO network.messages ( account_from_id, account_to_id, message) VALUES (1, 2, 'text4');
INSERT INTO network.messages ( account_from_id, account_to_id, message) VALUES (2, 1, 'text5');

commit;
-----------------------------------------